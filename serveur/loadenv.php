<?php
function parseEnv($filePath)
{
    if (!file_exists($filePath)) {
        throw new Exception('.env file not found');
    }

    $envVariables = [];
    $lines = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach ($lines as $line) {
        // Ignorer les commentaires
        if (strpos(trim($line), '#') === 0) {
            continue;
        }

        // Diviser la ligne en clé et valeur
        list($name, $value) = explode('=', $line, 2);

        // Supprimer les espaces autour de la clé et de la valeur
        $name = trim($name);
        $value = trim($value);

        // Stocker la variable dans le tableau
        $envVariables[$name] = $value;
    }

    return $envVariables;
}

?>