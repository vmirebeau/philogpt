<?php
// Désactiver l'output buffering et la compression
if (function_exists('apache_setenv')) {
    apache_setenv('no-gzip', '1'); // Désactiver la compression Apache
}
ini_set('output_buffering', 'off');
ini_set('zlib.output_compression', 'Off');
while (ob_get_level() > 0) {
    ob_end_flush();
}
ob_implicit_flush(true);

// Suppression du header Transfer-Encoding (évite les conflits)
header_remove("Transfer-Encoding");

// Suppression des headers par défaut éventuels
header_remove("Access-Control-Allow-Origin");

// Détection de l'origine pour CORS
$origin = isset($_SERVER['HTTP_ORIGIN']) ? rtrim($_SERVER['HTTP_ORIGIN'], '/') : '';
$origin_lower = strtolower($origin);
error_log("Origin reçu: " . $origin_lower);

$allowed_origins = array_map('strtolower', [
    'https://philogpt.vmirebeau.fr',
    'https://philogpt.local',
    'https://philogpt.local:3000'
]);

if (in_array($origin_lower, $allowed_origins)) {
    header("Access-Control-Allow-Origin: " . $origin);
    header("Access-Control-Allow-Credentials: true");
} else {
    header("Access-Control-Allow-Origin: null");
}

header("Vary: Origin");
header('Content-Type: text/event-stream'); // IMPORTANT : Indiquer SSE au lieu de JSON
header('Cache-Control: no-cache');
header('Connection: keep-alive');
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    exit(0);
}

// Lire la requête JSON envoyée par le client
$json_data = file_get_contents('php://input');
error_log("Payload reçu: " . $json_data);
$data = json_decode($json_data, true);

if (!isset($data['id']) || !isset($data['context']) || !isset($data['prompt'])) {
    http_response_code(400);
    echo "data: " . json_encode(["error" => "Les champs 'id', 'context' ou 'prompt' sont manquants."]) . "\n\n";
    exit;
}



// Tronquer le prompt aux 500 premiers caractères
//$data['prompt'] = substr($data['prompt'], 0, 500);

// Préparation du payload à envoyer au FastAPI
$payload = json_encode([
    "id"      => $data['id'],
    "context" => $data['context'],
    "prompt"  => $data['prompt']
]);

// URL du serveur FastAPI
$fastapi_url = "http://51.83.74.39:24857/chat";

// Initialisation de cURL
$ch = curl_init($fastapi_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, false); // Diffuser directement la réponse
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

// Fonction de rappel pour transmettre le flux de données au client au fur et à mesure
// Fonction de rappel pour transmettre le flux de données au client au fur et à mesure
curl_setopt($ch, CURLOPT_WRITEFUNCTION, function($ch, $data) {
    // Si le chunk commence déjà par "data: ", on l'envoie directement
    //if (strpos($data, "data: ") === 0) {
        echo $data;
    //} else {
        // Sinon, on ajoute le préfixe SSE
      //  echo "data: " . trim($data) . "\n\n";
    //}
    flush();
    return strlen($data);
});

// Exécuter la requête cURL
$result = curl_exec($ch);
if (curl_errno($ch)) {
    $error_msg = curl_error($ch);
    http_response_code(500);
    // Envoyer un événement SSE d'erreur
    echo "event: error\n";
    echo "data: " . json_encode(["error" => "Erreur cURL : " . $error_msg]) . "\n\n";
    flush();
    curl_close($ch);
    exit;
}

// Assurer la fin correcte du flux
//echo "data: [DONE]\n\n"; // Marqueur de fin SSE
flush();

curl_close($ch);
?>
