// SettingsPopup.js
import React, { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import './ComposantInputBar.css';

/**
 * Composant SettingsPopup
 *
 * @param {Object} props
 * @param {boolean} props.isOpen - Contrôle la visibilité du popup.
 * @param {function} props.setIsOpen - Fonction pour ouvrir/fermer le popup.
 * @param {object} props.buttonRef - Référence du bouton déclencheur.
 * @param {ReactNode} props.children - Contenu à afficher dans le popup.
 */
function SettingsPopup({ isOpen, setIsOpen, buttonRef, children }) {
  const popupRef = useRef(null);

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        popupRef.current &&
        !popupRef.current.contains(event.target) &&
        buttonRef.current &&
        !buttonRef.current.contains(event.target)
      ) {
        setIsOpen(false);
      }
    }

    if (isOpen) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isOpen, setIsOpen, buttonRef]);

  useEffect(() => {
    function handleResize() {
      setIsOpen(false);
    }

    if (isOpen) {
      window.addEventListener('resize', handleResize);
    }

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [isOpen, setIsOpen]);

  // Calculer la position du popup basé sur la position du bouton
  const calculatePosition = () => {
    if (buttonRef.current) {
      const rect = buttonRef.current.getBoundingClientRect();
      return { top: rect.top + window.scrollY, left: rect.left + window.scrollX };
    }
    return { top: 0, left: 0 };
  };

  const { top, left } = calculatePosition();

  if (!isOpen) return null;

  return ReactDOM.createPortal(
    <div
      className={`popupDiv parametresEleve ${isOpen ? 'show' : 'hide'}`}
      ref={popupRef}
      style={{
        position: 'absolute',
        top: `${top - 240}px`,  // Ajustez selon vos besoins
        left: `${left - 10}px`, // Ajustez selon vos besoins
        zIndex: 1000
      }}
    >
      {children}
    </div>,
    document.body
  );
}

export default SettingsPopup;
