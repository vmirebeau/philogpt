// ComposantInputBar.js
import React from 'react';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import './ComposantInputBar.css';

const levels = ["Débutant", "Intermédiaire", "Avancé", "Expert"];
const lengths = ["Une phrase", "Un paragraphe", "Une page", "Un cours"];

/**
 * ComposantInputBar
 * 
 * @param {Object} props
 * @param {object} props.userInfo - Les infos de l’utilisateur (niveau, longueur, etc.)
 * @param {function} props.setUserInfo - Permet de mettre à jour les infos de l’utilisateur
 * @param {function} props.setUpdateUser - Fonction qui signale que l'utilisateur doit être mis à jour
 */
function ComposantInputBar({ userInfo, setUserInfo }) {
  return (
    <>
      {/* Niveau scolaire */}
      <div className="slider-container">
        <div className="slider-title">
          Niveau de la réponse : <strong>{levels[userInfo.niveau_reponses]}</strong>
        </div>
        <Slider
          min={0}
          max={levels.length - 1}
          step={1}
          value={userInfo.niveau_reponses}
          onChange={(value) => {
            setUserInfo((prev) => ({ ...prev, niveau_reponses: value }));
            //setUpdateUser(true);
          }}
          marks={{
            0: "Débutant",
            1: "Intermédiaire",
            2: "Avancé",
            3: "Expert"
          }}
          included={false}
        />
      </div>

      {/* Longueur de la réponse */}
      <div className="slider-container">
        <div className="slider-title">
          Longueur de la réponse : <strong>{lengths[userInfo.longueur_reponses]}</strong>
        </div>
        <Slider
          min={0}
          max={lengths.length - 1}
          step={1}
          value={userInfo.longueur_reponses}
          onChange={(value) => {
            setUserInfo((prev) => ({ ...prev, longueur_reponses: value }));
            //setUpdateUser(true);
          }}
          marks={{
            0: "Une phrase",
            1: "Un paragraphe",
            2: "Une page",
            3: "Un cours"
          }}
          included={false}
        />
      </div>
    </>
  );
}

export default ComposantInputBar;
