import { useState, useEffect, useRef } from "react";
import { createPortal } from "react-dom";
import ConfirmationModal from "./ConfirmationModal";
import exportAll from "./ExportAll";


import "./Header.css";
import AboutModal from "./AboutModal";

const Header = ({
  philosopher,
  setShowInfos,
  setSidebarVisible,
  chatPrompts,
  setIdShowInfosWhenGroup,
  messages,
  setMessages,
  setId,
  allMessages,
  setAllMessages,
  smallImages,
  setMustSaveData
}) => {
  const [openMenu, setOpenMenu] = useState(false);
  const [menuStyle, setMenuStyle] = useState({});
  const buttonRef = useRef(null);
  const menuRef = useRef(null);

  const [showConfirmation, setShowConfirmation] = useState(false); // Etat pour l'affichage du modal
  const [showConfirmationSupprimer, setShowConfirmationSupprimer] = useState(false); // Etat pour l'affichage du modal de suppression
  const [showAbout, setShowAbout] = useState(false); // Etat pour le modal "à propos"

  const handleClickOutside = (event) => {
    if (
      menuRef.current &&
      !menuRef.current.contains(event.target) &&
      buttonRef.current &&
      !buttonRef.current.contains(event.target)
    ) {
      setOpenMenu(false);
    }
  };

  useEffect(() => {
    if (openMenu) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [openMenu]);

  const multi = philosopher ? (philosopher.id >= 100 ? true : false) : false;

  useEffect(() => {
    if (buttonRef.current) {
      const rect = buttonRef.current.getBoundingClientRect();
      setMenuStyle({
        top: rect.bottom + window.scrollY,
        left: rect.left + window.scrollX - 200,
        width: rect.width + 200,
      });
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      setOpenMenu(false);
      if (buttonRef.current) {
        const rect = buttonRef.current.getBoundingClientRect();
        setMenuStyle({
          top: rect.bottom + window.scrollY,
          left: rect.left + window.scrollX - 200,
          width: rect.width + 200,
        });
      }
    };

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    const updateMenuPosition = () => {
      if (buttonRef.current) {
        const rect = buttonRef.current.getBoundingClientRect();
        setMenuStyle({
          top: rect.bottom + window.scrollY,
          left: rect.left + window.scrollX - 200,
          width: rect.width + 200,
        });
      }
    };

    // Mettre à jour la position du menu lorsque openMenu change
    updateMenuPosition();

    // Ajouter un écouteur d'événements pour recalculer la position si le bouton est redimensionné ou déplacé
    window.addEventListener('resize', updateMenuPosition);

    // Nettoyage de l'écouteur d'événements
    return () => {
      window.removeEventListener('resize', updateMenuPosition);
    };
  }, [openMenu, buttonRef]);


  const menuPortalElement = document.getElementById('menu-portal') || document.createElement('div');
  if (!menuPortalElement.id) {
    menuPortalElement.id = 'menu-portal';
    document.body.appendChild(menuPortalElement);
  }

  const handleConfirmEffacer = () => {
    if (philosopher.id >= 100) {
      setMessages([{ role: 'interface', content: 'Début de la conversation', date: new Date().toISOString() }]);
    } else {
      setMessages([{ role: "assistant", content: philosopher.greeting, date: new Date().toISOString() }]);
    }
    //setAllMessages([]); // on efface tous les messages
    setMustSaveData(true);
    setShowConfirmation(false);
  };

  const handleConfirmSupprimer = () => {
    // Filtrer pour supprimer l'entrée correspondant à philosopher.id : on prend tous les messages qui ne sont pas le groupe en cours, et on l'affectera à allMessages
    const newDataChat = allMessages.filter(item => item.id !== philosopher.id);

    // Trouver l'entrée avec la date la plus récente dans les messages. Ca servira pour mettra le bon id, étant donné que l'id actif précédent est supprimé
    let latestEntry = null;
    let latestDate = null;

    newDataChat.forEach(entry => {
      entry.messages.forEach(message => {
        const messageDate = new Date(message.date);
        if (!latestDate || messageDate > latestDate) {
          latestDate = messageDate;
          latestEntry = entry;
        }
      });
    });

    // Mettre à jour le dataChat et l'ID
    setAllMessages(newDataChat);

    // Si une entrée avec une date la plus récente a été trouvée, définir son ID
    if (latestEntry) {
      setId(latestEntry.id);
    } else {
      // Si newDataChat est vide, définir un ID par défaut, par exemple 0
      setId(0);
    }

    // Fermer la modal
    setMustSaveData(true);
    setShowConfirmationSupprimer(false);
  };

  const handleCancel = () => {
    setShowConfirmation(false);
    setShowConfirmationSupprimer(false);

  };

  return (
    <div className="header" onClick={() => {
      setShowInfos(true);
      if (philosopher?.id >= 100) {
        setIdShowInfosWhenGroup(-1);
      }
    }}>
      <div className="profilHeader">
        <div onClick={(e) => { e.stopPropagation(); setSidebarVisible(true); }} className="mobileShow">
          <svg width="30" height="40" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" fill="none" stroke="black" strokeWidth="3">
            <line x1="25" y1="20" x2="5" y2="20" />
            <polyline points="15,10 5,20 15,30" />
          </svg>
        </div>

        {philosopher && (
          <>
            {multi ? (
              <>
                <div className="multiImageContainer">
                  {philosopher.philosophes.map(id => (
                    <img key={id} src={smallImages[id]} className="profilePicHaut multiImage" alt="Profile" />
                  ))}
                </div>
                <div><div style={{ flexDirection: 'column' }}>
                  <div className="nomProfilHeader">Discussion de groupe</div>
                  <div className="descProfilHeader">
                    {philosopher.philosophes // on va chercher le nom du philosophe en fonction de son id
                      .map(id => {
                        const prompt = chatPrompts.find(prompt => prompt.id === id);
                        return prompt ? prompt.shortnom : '';
                      })
                      .filter(Boolean)
                      .join(", ")}, Vous
                  </div>
                </div>
                </div>
              </>
            ) : ( // si ce n'est pas un groupe
              <>
                <img src={smallImages[philosopher.id]} className="profilePicHaut" alt="Profile" />
                <div>
                  <div className="nomProfilHeader">{philosopher.nom}</div>
                  <div className="descProfilHeader">
                    {philosopher.lieu + (philosopher.lieu !== "" ? ", " : "")}
                    {philosopher.dates}
                  </div>
                </div>
              </>
            )}
          </>
        )}
      </div>

      <div
        className={`boutonMenu ${openMenu ? "menuActif" : ""}`}
        onClick={(e) => { e.stopPropagation(); setOpenMenu(!openMenu); }}
        ref={buttonRef}
      >
        <svg viewBox="0 0 24 24" height="24" width="24" preserveAspectRatio="xMidYMid meet" className="" version="1.1" x="0px" y="0px" enableBackground="new 0 0 24 24"><title>menu</title><path fill="currentColor" d="M12,7c1.104,0,2-0.896,2-2c0-1.105-0.895-2-2-2c-1.104,0-2,0.894-2,2 C10,6.105,10.895,7,12,7z M12,9c-1.104,0-2,0.894-2,2c0,1.104,0.895,2,2,2c1.104,0,2-0.896,2-2C13.999,9.895,13.104,9,12,9z M12,15 c-1.104,0-2,0.894-2,2c0,1.104,0.895,2,2,2c1.104,0,2-0.896,2-2C13.999,15.894,13.104,15,12,15z"></path></svg>
      </div>

      {createPortal(
        <div
          className={`menu ${openMenu ? "displayMenu" : "hideMenu"}`}
          style={menuStyle}
          ref={menuRef}
        >
          <div className="menu-item" onClick={(e) => { e.stopPropagation(); setOpenMenu(false); exportAll(messages, philosopher, chatPrompts) }}>Exporter la discussion</div>
          <div className="menu-item" onClick={(e) => { e.stopPropagation(); setOpenMenu(false); setShowConfirmation(true); }}>Effacer la discussion</div>
          {philosopher?.id >= 100 &&
            <div className="menu-item" onClick={(e) => { e.stopPropagation(); setOpenMenu(false); setShowConfirmationSupprimer(true); }}>Supprimer la discussion</div>
          }
          <div className="menu-item" onClick={(e) => { e.stopPropagation(); setOpenMenu(false); setShowAbout(true) }}>A propos</div>
        </div>,
        menuPortalElement
      )}

      {showConfirmation && (
        <ConfirmationModal
          titre={"Effacer cette discussion ?"}
          texteBouton="Effacer la discussion"
          onConfirm={handleConfirmEffacer}
          onCancel={handleCancel}
        />
      )}
      {showConfirmationSupprimer && (
        <ConfirmationModal
          titre={"Supprimer ce groupe ?"}
          texteBouton="Supprimer le groupe"
          onConfirm={handleConfirmSupprimer}
          onCancel={handleCancel}
        />
      )}
      {showAbout && (
        <AboutModal
          onClose={() => setShowAbout(false)}
        />
      )}
    </div>
  );
};

export default Header;