import React, { useState, useEffect, useRef } from "react";
import "./MessageInput.css";
import { ReactComponent as PicsSettings } from "../pics/settings.svg";
import SettingsPopup from "./SettingsPopup";
import ComposantInputBar from "./ComposantInputBar";


function MessageInput({ onSendMessage, thinking, generating, stopAll, inputUser, setInputUser, userInfo, setUserInfo }) {
  const [envoyerActif, setEnvoyerActif] = useState(false);
  const textareaRef = useRef(null);
  const [showModalParametres, setShowModalParametres] = useState(false);

  const buttonRef = useRef(null);


  // Gestion de l'envoi de message ou de l'arrêt du traitement en cours
  const handleSend = () => {
    if (thinking || generating) {
      if (stopAll) {
        stopAll(); // Arrêter le traitement du message en cours
      }
    } else {
      if (inputUser.trim()) {
        onSendMessage(inputUser);
        setInputUser("");
      }
    }
  };

  // Ajustement de la hauteur du textarea en fonction de son contenu
  const handleChange = (e) => {
    setInputUser(e.target.value);
    adjustTextareaHeight();
  };

  // Gestion de l'envoi par la touche "Enter"
  const handleKeyPress = (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
      e.preventDefault();
      handleSend();
    }
  };

  // Ajustement automatique de la hauteur du textarea
  const adjustTextareaHeight = () => {
    const textarea = textareaRef.current;
    textarea.style.height = "auto";
    textarea.style.height = `${Math.min(textarea.scrollHeight, 10 * 20)}px`; // Limite à 10 lignes
  };

  // Mise à jour de l'état d'activation du bouton
  useEffect(() => {
    setEnvoyerActif(!(inputUser === "") || thinking || generating);
    adjustTextareaHeight(); // Ajuste la hauteur initiale
  }, [inputUser, thinking, generating]);

  return (
    <div className="inputzone">
      {/* Bouton Paramètres */}
      <div style={{ position: 'relative', display: 'flex', marginRight:'10px' }}>
            <div
              className="parametresChat"
              onClick={() => setShowModalParametres(!showModalParametres)}
              ref={buttonRef}
            >
              <PicsSettings
                height={32}
                width={32}
                className="boutonParametresEleve"
                alt="Paramètres du chatbot"
              />
            </div>
          </div>

      <textarea
        ref={textareaRef}
        placeholder="Posez votre question..."
        value={inputUser}
        maxLength={500}
        onChange={handleChange}
        onKeyPress={handleKeyPress}
        rows={1} // Nombre de lignes initial
        style={{ maxHeight: "200px", overflow: "auto" }} // Limite de hauteur à 10 lignes (20px par ligne)
      />
      <div
        className={`fleche ${envoyerActif ? "active" : "inactive"}`}
        onClick={handleSend}
      >
        {!(thinking || generating) ? (
          <>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              fill="none"
              viewBox="0 0 32 32"
              className="icon-2xl"
            >
              <path
                fill="currentColor"
                d="M15.192 8.906a1.143 1.143 0 0 1 1.616 0l5.143 5.143a1.143 1.143 0 0 1-1.616 1.616l-3.192-3.192v9.813a1.143 1.143 0 0 1-2.286 0v-9.813l-3.192 3.192a1.143 1.143 0 1 1-1.616-1.616z"
              ></path>
            </svg>
          </>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            fill="none"
            viewBox="0 0 32 32"
            className="icon-lg"
          >
            <rect width="12" height="12" x="10" y="10" fill="currentColor" rx="1.25"></rect>
          </svg>
        )}
      </div>
            {/* Popup Paramètres via SettingsPopup */}
            <SettingsPopup
        isOpen={showModalParametres}
        setIsOpen={setShowModalParametres}
        buttonRef={buttonRef}
      >
        <ComposantInputBar
          userInfo={userInfo}
          setUserInfo={setUserInfo}
        />
      </SettingsPopup>
    </div>
  );
}

export default MessageInput;
