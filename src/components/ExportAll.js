import { saveAs } from 'file-saver';
import { Document, Packer, Paragraph, TextRun, AlignmentType } from 'docx';

// On parse le markdown
const parseFormattedText = (text) => {
  const parts = text.split(/(\*\*.*?\*\*|\*.*?\*)/); // On split le gras (**) ou l'italique (*)

  return parts.map((part) => {
    if (part.startsWith('**') && part.endsWith('**')) {
      // Si c'était entre **, on applique le gras
      return new TextRun({ text: part.slice(2, -2), bold: true });
    } else if (part.startsWith('*') && part.endsWith('*')) {
      // Si c'était entre *, on applique l'italique
      return new TextRun({ text: part.slice(1, -1), italics: true });
    }
    // Pour le reste, on renvoie du texte normal
    return new TextRun({ text: part });
  });
};


// On parse les headers (###) et les listes (-)
const parseContent = (content) => {
  const paragraphs = [];
  const lines = content.split('\n');

  lines.forEach(line => {
    if (line.startsWith('###')) {
      // On gère les headers
      paragraphs.push(new Paragraph({
        children: [new TextRun({ text: line.replace('###', '').trim(), bold: true, size: 28 })],
        spacing: { after: 100 },
      }));
    } else if (line.startsWith('- ')) {
      // On gère les listes
      paragraphs.push(new Paragraph({
        children: parseFormattedText(line.replace('- ', '').trim()), // Gras et italique
        bullet: { level: 0 }, // On crée les listes
        spacing: { after: 50 },
        alignment: AlignmentType.LEFT, // Les listes sont alignées à gauche
      }));
    } else {
      // Les paragraphes de textes normaux ont un parsing gras et italique
      paragraphs.push(new Paragraph({
        children: parseFormattedText(line), // On parse le gras et l'italique
        alignment: AlignmentType.LEFT, // Texte normal à gauche
        spacing: { after: 0 }, // Pas d'espace vertical
      }));
    }
  });

  return paragraphs;
};

// On crée un paragraphe pour chaque message
const createMessageParagraph = (sender, content) => {
  return [
    // Paragraphe pour le nom de l'interlocuteur
    new Paragraph({
      children: [
        new TextRun({
          text: `${sender}: `,
          underline: true,
          bold: true,
        }),
      ],
      alignment: AlignmentType.LEFT, // Alignement à gauche
      spacing: {
        before:500,
        after: 100, // Une peu d'espace entre le nom et le message
      },
    }),
    // Un paragraphe pour le contenu du message
    ...parseContent(content),
  ];
};

// Créer et exporter le fichier .docx
const exportAll = (messages, philosopher, chatPrompts) => {
  //const allMessages = oldMessagesPhilosopher.concat(messages);

  // On récupère les noms des participants
  let title;
  if (philosopher.id < 100) {
    const interlocutor = chatPrompts.find(philo => philo.id == philosopher.id)?.nom;
    title = `Discussion entre ${interlocutor} et vous`;
  } else {
    const groupMembers = philosopher.philosophes.map(id => chatPrompts[id].nom).join(", ");
    title = `Discussion entre ${groupMembers} et vous`;
  }

  // Création du contenu du document
  const doc = new Document({
    sections: [
      {
        children: [
          // Le titre est en gras, gros, centré
          new Paragraph({
            children: [
              new TextRun({
                text: title,
                bold: true,
                size: 32,
              }),
            ],
            alignment: AlignmentType.CENTER,
            spacing: {
              after: 400, // Espace après le titre
            },
          }),
          // On ajoute chaque message sous la forme d'un paragraphe
          ...messages
            .filter(msg => msg.role === "user" || msg.role === "assistant") // On n'inclut que les messages à montrer
            .flatMap(msg => {
              const sender = msg.role === "user" ? "Vous" : msg.name || philosopher.nom;
              return createMessageParagraph(sender + " ", msg.content);
            }),
        ],
      },
    ],
  });

  // Créer et télécharger le fichier .docx
  Packer.toBlob(doc).then((blob) => {
    saveAs(blob, "discussion.docx");
  });
};

export default exportAll;
