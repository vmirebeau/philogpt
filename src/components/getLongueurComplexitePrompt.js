
const promptNiveauReponse = [
    "Réponds avec des mots et des structures très simples.",
    "Réponds en simplifiant un peu le propos.",
    "Réponds en élaborant correctement tes réponses.",
    "Réponds de façon aussi précise et élaborée que possible."
];

const promptLongueurReponses = [
    "Ta réponse doit tenir en une seule phrase, ne fais pas plus long, sous aucun prétexte.",
    "Ta réponse doit faire quelques phrases au maximum. Ne dépasse pas cette longueur.",
    "Tu peux prendre deux paragraphes au maximum pour construire ta réponse.",
    "Tu peux prendre trois paragraphes au maximum pour construire ta réponse."
];

const getLongueurComplexitePrompt = (userInfo) => {

        return `${promptNiveauReponse[userInfo.niveau_reponses]} ${promptLongueurReponses[userInfo.longueur_reponses]}`

}

export default getLongueurComplexitePrompt;