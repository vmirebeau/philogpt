import React, { useMemo, useEffect } from "react";
import "./Sidebar.css";
import FlipMove from "react-flip-move";

const escapeRegExp = (string) => {
  if (typeof string !== 'string') {
    return ''; // Renvoie une chaîne vide si ce n'est pas une chaîne valide
  }
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& signifie la chaîne entière correspondante
};

const highlightText = (text, highlight) => {
  if (typeof text !== 'string' || typeof highlight !== 'string' || !highlight.trim()) {
    return text; // Renvoie le texte inchangé si l'un des paramètres est invalide
  }

  // Utilisation de la fonction pour échapper les caractères spéciaux
  const escapedHighlight = escapeRegExp(highlight);
  const regex = new RegExp(`(${escapedHighlight})`, 'gi');
  
  // Si aucune correspondance n'est trouvée, on retourne directement le texte sans le modifier
  if (!regex.test(text)) {
    return text;
  }

  const parts = text.split(regex);

  return parts.map((part, index) =>
    part.toLowerCase() === highlight.toLowerCase()
      ? <span key={index} className="foundText">{part}</span>
      : part
  );
};

// Fonction pour retirer les caractères Markdown (*) et (**) d'une chaîne de caractères.
const removeMarkdown = (text) => {
  if (typeof text !== "string") return text;
  // Retire le formatage gras : **texte**
  let cleanedText = text.replace(/\*\*([^*]+)\*\*/g, "$1");
  // Retire le formatage italique : *texte*
  cleanedText = cleanedText.replace(/\*([^*]+)\*/g, "$1");
  return cleanedText;
};

const Sidebar = ({
  id,
  setId,
  chatPrompts,
  allMessages,
  searchText,
  filtreEpoque,
  idPhilosophes,
  onSelectPhilosophe,
  addGroupMode,
  setLatestId,
  smallImages,
}) => {

  const sortedChatData = useMemo(() => {
    let filteredData = allMessages.map((message) => {
      const matchingPrompt = chatPrompts.find((prompt) => prompt.id === message.id);
    
      return {
        ...message, // garde toutes les propriétés actuelles de `allMessages`
        nom: matchingPrompt ? matchingPrompt.nom : message.nom, // si `matchingPrompt` existe, on prend son nom
        categorie: matchingPrompt ? matchingPrompt.categorie : "Groupe", // si `matchingPrompt` existe, on prend sa catégorie, sinon on met "Groupe"
      };
    });
    // on filtre les données en fonction des filtres choisis par l'utilisateur
    filteredData = filteredData.map(philosopher => {
      const philosopherName = philosopher.nom || ""; // On s'assure que c'est une chaîne
      const matchesSearchText = searchText.trim() === "" || philosopherName.toLowerCase().includes(searchText.toLowerCase());
      const matchesEpoqueFilter = !filtreEpoque || filtreEpoque.length === 0 || filtreEpoque.includes(philosopher.categorie);
  
      const matchesGroupModeFilter = !addGroupMode || (philosopher.categorie !== "Autres" && philosopher.categorie !== "Groupe");
  
      return {
        ...philosopher,
        isVisible: matchesSearchText && matchesEpoqueFilter && matchesGroupModeFilter,
      };
    });
  
    filteredData.sort((a, b) => {
      const aLastMessageDate = a.messages && a.messages.length > 0
        ? new Date(a.messages[a.messages.length - 1].date)
        : new Date(0);
  
      const bLastMessageDate = b.messages && b.messages.length > 0
        ? new Date(b.messages[b.messages.length - 1].date)
        : new Date(0);
  
      return bLastMessageDate - aLastMessageDate;
    });
  
    return filteredData;
  }, [allMessages, searchText, filtreEpoque, addGroupMode]);
  

  // On met à jour le dernier ID après que les données aient été ordonnées et affichées
  useEffect(() => {
    if (sortedChatData.length > 0) {
      setLatestId(sortedChatData[0]?.id);
    }
  }, [sortedChatData, setLatestId]);

  const handleSelect = (philosopherId) => {
    if (addGroupMode) {
      onSelectPhilosophe(philosopherId);
    } else {
      setId(philosopherId);
    }
  };

  const formatTime = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
  };

  return (
    <div className="sidebar">
      <FlipMove enterAnimation="none" leaveAnimation="none">
        {sortedChatData.map((philosopher) => {

          const lastMessage = philosopher.messages && philosopher.messages.length > 0
            ? philosopher.messages[philosopher.messages.length - 1]
            : null;

          return (
            <div
              key={philosopher.id}
              className={`chatHistory ${(philosopher.id === id) && !addGroupMode ? "active" : "notactive"} ${philosopher.isVisible ? "visible" : "noheight"} ${addGroupMode && idPhilosophes.length > 2 && !idPhilosophes.includes(philosopher.id) ? "disabled" : ""}`}
              onClick={() => handleSelect(philosopher.id)}
            >
              <div className="divMainHistory">
                <div className={`profilePicMenu ${philosopher.categorie === "Groupe" ? "marginGroup" + philosopher.philosophes.length : ""}`}>
                  {philosopher.categorie === "Groupe" ? (
                    philosopher.philosophes.map(id => (
                      <img key={id} src={smallImages[id]} className="imgMenu multi multiImageBig" alt={`Profile ${chatPrompts[id].nom}`} />
                    ))
                  ) : (
                    <img src={smallImages[philosopher.id]} className="imgMenu" alt={`Profile ${philosopher.nom}`} />
                  )}
                  {addGroupMode && idPhilosophes.includes(philosopher.id) && (
                    <div className={`selected-checkmark`}>
                      <svg style={{ paddingTop: '4px' }} width="13" height="16" viewBox="0 0 13 16" xmlns="http://www.w3.org/2000/svg">
                        <polyline points="1,8 5,12 12,1" stroke="white" strokeWidth="2" fill="none" />
                      </svg>
                    </div>
                  )}
                </div>
                <div className={`nomEtMessage ${philosopher.categorie === "Groupe" ? "marginGroupText" + philosopher.philosophes.length : ''}`}>
                  <div className="containerHeure">
                    <div className={`philosopherName ${philosopher.unread ? "unreadMsg" : ""}`}>
                      {philosopher.categorie === "Groupe"
                        ? philosopher.philosophes.map(id => chatPrompts.find(philo => philo.id === id)?.shortnom).join(", ")
                        : highlightText(philosopher.nom, searchText)
                      }
                    </div>
                    <div className={`heure ${philosopher.unread ? "heureUnread" : ""}`}>
                      {lastMessage ? formatTime(lastMessage.date) : ""}
                    </div>
                  </div>
                  <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    <div className={`lastMessage ${lastMessage ? (lastMessage?.content?.trim() !== "" ? "" : "writingStyle") : ""} ${philosopher.unread ? "unreadIntervention" : ""}`}>
                      {lastMessage 
                        ? (lastMessage?.content?.trim() !== "" 
                            ? removeMarkdown(lastMessage.content).substring(0, 110) 
                            : "écrit...")
                        : "Aucun message pour l'instant"}
                    </div>
                    {philosopher.unread && <div className="nombreMessage">1</div>}
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </FlipMove>
    </div>
  );
};

export default Sidebar;
