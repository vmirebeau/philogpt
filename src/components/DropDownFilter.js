import React, { useState, useRef, useEffect } from "react";
import ReactDOM from "react-dom";
import "./DropDownFilter.css";

const options = [
    { label: "Antiquité", value: "Antiquité" },
    { label: "Renaissance", value: "Renaissance" },
    { label: "Classiques", value: "Classiques" },
    { label: "Modernes", value: "Modernes" },
    { label: "Contemporains", value: "Contemporains" },
    { label: "Autres", value: "Autres" },
    { label: "Groupes", value: "Groupe" },
];

const DropdownFilter = ({ setFiltreEpoque, addGroupMode }) => {
    const [isOpen, setIsOpen] = useState(false);
    const [isClosing, setIsClosing] = useState(false);
    const [selectedOptions, setSelectedOptions] = useState([]);
    const dropdownRef = useRef(null);
    const buttonRef = useRef(null);
    const [dropdownStyles, setDropdownStyles] = useState({});

    const toggleDropdown = () => {
        if (isOpen) {
            setIsClosing(true);
            setTimeout(() => {
                setIsClosing(false);
                setIsOpen(false);
            }, 300); // Durée de l'animation CSS
        } else {
            setIsOpen(true);
        }
    };

    const handleOptionClick = (option) => {
        let updatedOptions;
        if (selectedOptions.includes(option.value)) {
            updatedOptions = selectedOptions.filter((item) => item !== option.value);
        } else {
            updatedOptions = [...selectedOptions, option.value];
        }

        // Si tous les filtres sont sélectionnés, les désélectionner tous
        if (updatedOptions.length === options.length) {
            updatedOptions = [];
        }

        setSelectedOptions(updatedOptions);
        setFiltreEpoque(updatedOptions);
    };

    const clearFilters = (e) => {
        e.stopPropagation(); // Empêche la fermeture du dropdown lors du clic sur la croix
        setSelectedOptions([]);
        setFiltreEpoque([]);
    };

    useEffect(() => {
        if (buttonRef.current) {
            const rect = buttonRef.current.getBoundingClientRect();
            setDropdownStyles({
                position: 'absolute',
                visibility: isOpen ? "visible" : "hidden",
                top: rect.bottom + window.scrollY,
                left: rect.left + window.scrollX,
                width: rect.width,
            });
        }
    }, [isOpen]);

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (
                dropdownRef.current &&
                !dropdownRef.current.contains(event.target) &&
                buttonRef.current &&
                !buttonRef.current.contains(event.target)
            ) {
                setIsOpen(false);
            }
        };
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [isOpen]);

    
    useEffect(() => {
        const handleResize = () => {
          setIsOpen(false); // Fermer le menu
        };
      
        window.addEventListener('resize', handleResize);
        
        return () => {
          window.removeEventListener('resize', handleResize);
        };
      }, []);

    const dropdownContent = (
        <div
            className={`dropdown-content ${isClosing ? "hideMenu" : isOpen ? "displayMenu" : ""}`}
            style={dropdownStyles}
            ref={dropdownRef}
        >
            {options.map((option) => (
                <>
                {!((option.value === "Groupe" || option.value === "Autres") && addGroupMode) &&
                <div
                    key={option.value}
                    className="dropdown-option"
                    onClick={() => handleOptionClick(option)}
                >
                    <span className={option.value === "Groupe" ? "menuGroupes" : ""}>{option.label}</span>
                    {selectedOptions.includes(option.value) && (
                        <svg
                            viewBox="0 0 24 24"
                            height="16"
                            width="16"
                            fill="currentColor"
                            className="checkmark-svg"
                        >
                            <path d="M9 16.2l-4.2-4.2 1.4-1.4L9 13.4l7.8-7.8 1.4 1.4z" />
                        </svg>
                    )}
                </div>
                }
                </>
            ))}
        </div>
    );
    

    return (
        <div className="dropdown-container">
            <button onClick={toggleDropdown} className="dropbtn" ref={buttonRef}>
                {`Filtrer${selectedOptions.length > 0 ? ` (${selectedOptions.length})` : ""}`}
                <svg
                    viewBox="0 0 24 24"
                    height="24"
                    width="24"
                    fill="currentColor"
                    className={`chevron-svg ${isOpen ? "rotate" : ""}`}
                >
                    <path d="M7 10l5 5 5-5z" />
                </svg>
            </button>
            <div
                className={`searchTag bleu ${selectedOptions.length === 0 ? "disabled" : ""}`}
                onClick={selectedOptions.length > 0 ? clearFilters : null}
            >
                Tout voir
            </div>
            {ReactDOM.createPortal(dropdownContent, document.body)}
        </div>
    );
};

export default DropdownFilter;
