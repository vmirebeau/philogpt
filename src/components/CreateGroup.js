import React, { useRef } from "react";
import FlipMove from "react-flip-move";
import "./CreateGroup.css";

const CreateGroup = ({ idPhilosophes, chatPrompts, setIdPhilosophes, smallImages}) => {
  const groupRef = useRef(null);


  const handleRemove = (id) => {
    setIdPhilosophes(prev => prev.filter(philoId => philoId !== id));
  };

  return (
    <div ref={groupRef} className="create-group">
      {idPhilosophes.length === 0 &&
      <span className="msgGroupe"><div className="msgInfo">Cliquez sur des philosophes<br />pour les ajouter au groupe</div></span>
      }
      <FlipMove
        duration={200}
        easing="ease-in-out"
        staggerDurationBy={0}
        staggerDelayBy={0}

      >
        {idPhilosophes.map(id => {
          const philosopher = chatPrompts.find(philo => philo.id === id);
          return (
            <div
              key={id}
              className="selected-philosopher"
              onClick={() => handleRemove(id)}
            >
              <div className="philosopher-image">
                <img src={smallImages[id]} alt={philosopher.nom} />
                <div className="remove-icon">
                  <svg width="10" height="10" xmlns="http://www.w3.org/2000/svg">
                    <line x1="0" y1="0" x2="10" y2="10" stroke="white" strokeWidth="2" />
                    <line x1="0" y1="10" x2="10" y2="0" stroke="white" strokeWidth="2" />
                  </svg>
                </div>
              </div>
              <div className="philosopher-name">{philosopher.shortnom}</div>
            </div>
          );
        })}
      </FlipMove>
    </div>
  );
};

export default CreateGroup;
