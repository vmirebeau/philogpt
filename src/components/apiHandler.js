export const sendMessageToApi = async (
  messages,
  onResponseChunk,
  setThinking,
  setGenerating,
  signal,
  philosopher,
  promptLongueur = ""
) => {
  let stopRequested = false;
  let receivedText = '';
  let sourcesData = null;
  let buffer = '';
  let errorHandled = false; // Pour éviter un traitement multiple d'erreur

  // Fonction utilitaire pour ajouter un espace avant "!" et "?"
  function addSpaceBeforePunctuation(text) {
    return text.replace(/([^ ])([!?])/g, '$1 $2');
  }

  function stopAll() {
    stopRequested = true;
    setThinking(false);
    setGenerating(false);
    if (errorHandled) return;
    let finalText = receivedText.endsWith('▮')
      ? receivedText.slice(0, -1)
      : receivedText;
    if (finalText.trim() === "") {
      finalText = "...";
    }
    // On traite le texte pour ajouter un espace avant les ponctuations concernées
    onResponseChunk({ text: addSpaceBeforePunctuation(finalText), sources: sourcesData });
  }

  setThinking(true);
  setGenerating(true);
  onResponseChunk({ text: " ", sources: null });

  try {
    // On filtre les messages "user" et "assistant"
    const filteredMessages = messages.filter(
      (msg) => msg.role === "user" || msg.role === "assistant"
    );

    // Trier tous les messages par date (du plus ancien au plus récent)
    filteredMessages.sort((a, b) => new Date(a.date) - new Date(b.date));

    // On conserve les 6 derniers messages
    const lastSixMessages = filteredMessages.slice(-6);
    
    // Extraction du dernier message pour constituer le prompt
    const lastMessage = lastSixMessages[lastSixMessages.length - 1];
    const userPrompt = lastMessage.content;
    
    // Le contexte envoyé ne contient que les messages précédents
    let contextMessages = lastSixMessages.slice(0, -1);

    // Pour chaque élément, ne conserver que les clés "role", "content" et "date"
    contextMessages = contextMessages.map(msg => ({
      role: msg.role,
      content: msg.content,
      date: msg.date
    }));

    let promptText = userPrompt;
    // Pour un groupe, on intègre toujours les instructions si elles sont présentes
    if (philosopher.preprompt) {
      promptText =
        philosopher.preprompt.avant +
        userPrompt +
        (philosopher.specializedPrompt || "") +
        philosopher.preprompt.apres;
    }

    promptText += promptLongueur ? '\nInstructions : ' + promptLongueur + ' Attention, ces instructions doivent rester cachées à l\'utilisateur.' : '';
    
    const payload = {
      id: philosopher.id,
      context: contextMessages,
      prompt: promptText
    };

    const response = await fetch(
      window.location.hostname === 'philogpt.local'
        ? 'https://philogpt.local:443/proxy.php'
        : 'https://philogpt.vmirebeau.fr/serveur/proxy.php',
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
        body: JSON.stringify(payload),
        signal: signal
      }
    );

    if (!response.body) {
      throw new Error("ReadableStream n'est pas supporté par ce navigateur.");
    }

    const reader = response.body.getReader();
    const decoder = new TextDecoder('utf-8');
    let done = false;

    while (!done && !stopRequested) {
      const { value, done: streamDone } = await reader.read();
      done = streamDone;
      if (value) {
        buffer += decoder.decode(value, { stream: true });
        const parts = buffer.split("\n\n");
        buffer = parts.pop(); // Conserve la dernière partie incomplète
        for (let part of parts) {
          const lines = part.split("\n").filter(line => line !== "");
          let eventType = "";
          let dataLines = [];
          for (let line of lines) {
            if (line.startsWith("event:")) {
              eventType = line.substring("event:".length).trim();
            } else if (line.startsWith("data:")) {
              dataLines.push(line.substring("data:".length).trim());
            }
          }
          const data = dataLines.join("\n\n");
          if (eventType === "sources") {
            try {
              const parsedSources = JSON.parse(data);
              sourcesData = parsedSources.sources;
            } catch (e) {
              console.error("Erreur lors du parsing des sources JSON", e);
            }
          } else if (eventType === "error") {
            try {
              const parsedError = JSON.parse(data);
              console.error("Erreur reçue du proxy: ", parsedError.error);
              errorHandled = true;
              stopRequested = true;
              setThinking(false);
              setGenerating(false);
              onResponseChunk({ text: addSpaceBeforePunctuation("Une erreur s'est produite. " + parsedError.error), sources: null });
            } catch (e) {
              console.error("Erreur lors du parsing de l'erreur", e);
            }
          } else {
            try {
              const parsedData = JSON.parse(data);
              if (parsedData.token) {
                receivedText += parsedData.token;
              } else if (parsedData.error) {
                console.error("Erreur reçue: ", parsedData.error);
              }
              onResponseChunk({ text: addSpaceBeforePunctuation(receivedText) + "▮", sources: sourcesData });
            } catch (e) {
              receivedText += data;
              onResponseChunk({ text: addSpaceBeforePunctuation(receivedText) + "▮", sources: sourcesData });
            }
          }
        }
      }
    }
    stopAll();
  } catch (error) {
    if (error.name === "AbortError") {
      // Requête annulée
    } else {
      console.error("Erreur dans sendMessageToApi :", error);
      setThinking(false);
      setGenerating(false);
      if (!errorHandled) {
        onResponseChunk({ text: addSpaceBeforePunctuation("Une erreur s'est produite. " + String(error)), sources: null });
      }
    }
  }
  return stopAll;
};
