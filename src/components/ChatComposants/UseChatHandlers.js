import { useCallback, useRef } from "react";
import { sendMessageToApi } from "../apiHandler";
import { getInstructionsGroupe, getSpecializedPrompt } from "../../consts/prompts";

export const useChatHandlers = ({
  messages, // l'ensemble des messages à afficher
  philosopher, // le philosophe ou le groupe
  setMessages, // fonction pour mettre à jour les messages
  stopAll,
  setStopAll,
  stopGen, // fonction pour arrêter la génération en cours
  setThinking, // pour l'état "en train de réfléchir"
  setGenerating, // pour l'état "en train de générer"
  chatPrompts, // données relatives aux prompts (pour les groupes)
  setReponsesEnCours, // indique si une réponse est en cours
  setMustSaveData, // indique s'il faut enregistrer les données
  promptLongueur // prompt Longueur et complexité
}) => {
  const handleSendMessage = useCallback(async (message) => {
    // Arrêter toute génération en cours
    stopGen();
    if (stopAll) {
      setReponsesEnCours(false);
      stopAll();
    }

    const controller = new AbortController();
    const signal = controller.signal;

    const stopAllFunc = () => {
      setMustSaveData(true);
      controller.abort();
      setReponsesEnCours(false);
      setThinking(false);
      setGenerating(false);
    };
    setStopAll(() => stopAllFunc);

    // Ajout du message utilisateur
    const newMessages = [
      ...messages,
      { role: "user", content: message, date: new Date().toISOString() },
    ];
    setMessages(newMessages);
    setReponsesEnCours(true);

    let currentMessages = newMessages;

    // Fonction utilitaire pour créer ou mettre à jour le message assistant
    // avec un délai de 1 seconde pour la première réponse.
    const createOrUpdateAssistantMessage = ({
      partialResponse,
      isGroup = false,
      currentPhilosopher = null,
    }) => {
      const baseMsg = {
        role: "assistant",
        content: partialResponse.text,
        date: new Date().toISOString(),
      };
      if (isGroup && currentPhilosopher) {
        baseMsg.name = currentPhilosopher.nom;
        baseMsg.imgId = currentPhilosopher.id;
      }
      if (partialResponse.sources) {
        baseMsg.sources = partialResponse.sources;
      }
      return baseMsg;
    };

    if (philosopher.id >= 100) {
      // Branche groupe
      const idsPhilosophes = philosopher.philosophes;
      const ordreAleatoire = idsPhilosophes.slice().sort(() => Math.random() - 0.5);

      const filterMessageKeys = (messages) =>
        messages
          .filter((msg) => msg.role === "user" || msg.role === "assistant")
          .map((msg) => ({
            content: msg.content,
            date: msg.date,
            role: msg.role,
          }));

      // Fonction d'envoi avec mise à jour en streaming et délai sur le premier message assistant
      const sendMessageWithUpdates = async ({
        currentMessages,
        currentPhilosopher,
        preprompt,
        specializedPrompt,
        isGroup = false,
      }) => {
        // Variables pour gérer le délai
        let assistantMessageExists = false;
        let assistantMessagePending = false;
        let accumulatedText = "";

        await sendMessageToApi(
          filterMessageKeys(currentMessages).slice(-8),
          (partialResponse) => {
            if (signal.aborted) return;
            // Préparer le message assistant en fonction de la réponse partielle
            const newAssistantMsg = createOrUpdateAssistantMessage({
              partialResponse,
              isGroup,
              currentPhilosopher,
            });

            if (!assistantMessageExists) {
              if (!assistantMessagePending) {
                // Première réponse : on démarre un délai d'1s avant de créer le message
                assistantMessagePending = true;
                accumulatedText = partialResponse.text;
                setTimeout(() => {
                  // Création du message avec le texte le plus récent accumulé
                  const delayedMsg = createOrUpdateAssistantMessage({
                    partialResponse: { text: accumulatedText, sources: partialResponse.sources },
                    isGroup,
                    currentPhilosopher,
                  });
                  const updatedMessages = [...currentMessages, delayedMsg];
                  setMessages(updatedMessages);
                  currentMessages = updatedMessages;
                  assistantMessageExists = true;
                  assistantMessagePending = false;
                }, 1000);
              } else {
                // Si un délai est déjà lancé, on met à jour le texte accumulé
                accumulatedText = partialResponse.text;
              }
            } else {
              // Une fois le message créé, on le met à jour immédiatement
              const updatedMessages = [...currentMessages];
              const updatedMsg = createOrUpdateAssistantMessage({
                partialResponse,
                isGroup,
                currentPhilosopher,
              });
              updatedMessages[updatedMessages.length - 1] = updatedMsg;
              setMessages(updatedMessages);
              currentMessages = updatedMessages;
            }
          },
          setThinking,
          setGenerating,
          signal,
          { ...currentPhilosopher, preprompt, specializedPrompt }
        );

        setMustSaveData(true);
        setReponsesEnCours(false);

        return currentMessages;
      };

      // Itération sur chaque participant du groupe
      for (let i = 0; i < ordreAleatoire.length; i++) {
        if (signal.aborted) break;

        const idPhilosophe = ordreAleatoire[i];
        const participantActuel = chatPrompts.find((p) => p.id === idPhilosophe);

        const promptGroup = getInstructionsGroupe(
          idPhilosophe,
          ordreAleatoire,
          chatPrompts,
          participantActuel.prompt
        );

        currentMessages = await sendMessageWithUpdates({
          currentMessages,
          currentPhilosopher: participantActuel,
          preprompt: promptGroup,
          specializedPrompt: getSpecializedPrompt(message, participantActuel),
          isGroup: true,
        });
      }
    } else {
      // Branche individuelle
      let assistantMessageExists = false;
      let assistantMessagePending = false;
      let accumulatedText = "";

      await sendMessageToApi(
        currentMessages,
        (partialResponse) => {
          if (signal.aborted) return;
          const newAssistantMsg = createOrUpdateAssistantMessage({ partialResponse });
          if (!assistantMessageExists) {
            if (!assistantMessagePending) {
              assistantMessagePending = true;
              accumulatedText = partialResponse.text;
              setTimeout(() => {
                const delayedMsg = createOrUpdateAssistantMessage({
                  partialResponse: { text: accumulatedText, sources: partialResponse.sources },
                });
                const updatedMessages = [...currentMessages, delayedMsg];
                setMessages(updatedMessages);
                currentMessages = updatedMessages;
                assistantMessageExists = true;
                assistantMessagePending = false;
              }, 1000);
            } else {
              accumulatedText = partialResponse.text;
            }
          } else {
            const updatedMessages = [...currentMessages];
            const updatedMsg = createOrUpdateAssistantMessage({ partialResponse });
            updatedMessages[updatedMessages.length - 1] = updatedMsg;
            setMessages(updatedMessages);
            currentMessages = updatedMessages;
          }
        },
        setThinking,
        setGenerating,
        signal,
        philosopher,
        promptLongueur
      );
      setMustSaveData(true);
      setReponsesEnCours(false);
    }
  }, [
    messages,
    philosopher,
    setMessages,
    stopAll,
    stopGen,
    chatPrompts,
    setThinking,
    setGenerating,
    setStopAll,
    setMustSaveData,
    setReponsesEnCours,
  ]);

  return { handleSendMessage };
};
