import React, { useCallback, forwardRef } from 'react';
import ReactMarkdown from 'react-markdown';
import LoadingDots from '../LoadingDots';
import './Message.css';
import { LeftTailSVG, RightTailSVG, DoubleCheckSVG } from './MessageSVGs'; // on déporte les SVG pour plus de lisibilité

//Affichage d'un message singulier
const Message = forwardRef(({ // on utilise forwardRef pour passer une ref à un composant fonctionnel
  msg,  //le message, avec les propriétés name, role, content, date
  philosopher,  // le philosophe (ou groupe)
  smallImages,  // le tableau des petites images
  handlePhilosopherClick, // fonction qui affiche les infos dans le volet de droite
  thinking,
  setShowSources,
  setCurrentSources
 }, // est-ce qu'on est en train de réfléchir ? (= initialisation + générationZ)
  ref) => { // la ref passée par MessageList

  const isGroupPhilosopher = philosopher && philosopher.categorie === 'Groupe';

  const showSources = (sources) => {
    //console.log ("thread_id",sources.thread_id);
    //console.log("run_id", sources.run_id);
    setCurrentSources(sources);
    setShowSources(true);
  }

  const memoizedHandleClick = useCallback(() => {
    if (msg.name) handlePhilosopherClick(msg.name);
  }, [msg.name, handlePhilosopherClick]);
// Avant d'utiliser .trim(), on s'assure que msg.content est bien une chaîne
const content = typeof msg.content === 'string' ? msg.content : '';

return (
  <div className={`message-container big${msg.role}`}>
    <div ref={ref} className={`message msg${msg.role} ${isGroupPhilosopher && msg.role === 'assistant' ? 'decalagesmall' : ''}`}>
      {msg.role === 'assistant' && msg.name && (
        <div className="philosopher-info" onClick={memoizedHandleClick}>
          <img src={smallImages[msg.imgId]} alt={msg.name} className="imagePhilosophe" />
        </div>
      )}
      <div className={msg.role}>
        {msg.role === 'assistant' && (
          <>
            <LeftTailSVG />
            <div className="nomPhilosophe" onClick={memoizedHandleClick}>
              {msg.name}
            </div>
          </>
        )}
        {content.trim() === '' ? (
          thinking ? <LoadingDots /> : null
        ) : (
          <div style={{display:'flex',flexDirection:'column', gap:'20px'}}>
            <ReactMarkdown>{content}</ReactMarkdown>
          </div>
        )}
        {msg.role === 'user' && <RightTailSVG />}
        {msg.role !== 'interface' && (
          <div className="heure">
            <div style={{display:'flex',flexDirection: 'column'}}>
              <div style={{ marginTop: '5px' }}>
                {(msg.sources && (msg.sources.length > 0)) && (
                  <span className="sources-indicator" onClick={() => showSources(msg.sources)}>
                    Voir les sources
                  </span>
                )}
              </div>
              <div>
                {msg.date ? new Date(msg.date).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' }) : ''}
                <span aria-label=" Lu " data-icon="msg-dblcheck" className="checkread">
                  <DoubleCheckSVG />
                </span>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  </div>
);

});

export default React.memo(Message); // Utilisez React.memo pour éviter les re-rendus inutiles
