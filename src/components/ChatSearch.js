import React, { useState, useEffect } from "react";
import "./ChatSearch.css";
import DropdownFilter from "./DropDownFilter";
import Sidebar from "./Sidebar";
import CreateGroup from "./CreateGroup";

const ChatSearch = ({ searchText, setSearchText, setFiltreEpoque, prompt, id, setId, chatPrompts, allMessages, setAllMessages, filtreEpoque, addGroupMode, setAddGroupMode, setSidebarVisible, setLatestId, smallImages, setMustSaveData }) => {
    const [idPhilosophes, setIdPhilosophes] = useState([]);

    useEffect(() => {
        setIdPhilosophes([]);
    }, [addGroupMode])

    const handleSelectPhilosophe = (philosopheId) => {
        if (idPhilosophes.includes(philosopheId)) {
            setIdPhilosophes(idPhilosophes.filter(id => id !== philosopheId));
        } else if (idPhilosophes.length < 3) {
            setIdPhilosophes([...idPhilosophes, philosopheId]);
        }
    };

    const handleCreateGroup = () => {
        // 1: On génère un id unique pour le nouveau groupe
        const highestId = allMessages.reduce((maxId, chat) => Math.max(maxId, chat.id), 0);
        const newId = highestId + 1 >= 100 ? highestId + 1 : 100; // on prend l'id max, sinon 100 si pas encore de groupe
      
        // 2: On crée une nouvelle entrée de groupe
        const newGroup = {
          id: newId,
          philosophes: idPhilosophes,
          messages: [{ role: 'interface', content: 'Début de la conversation', date: new Date().toISOString() }]
        };
      
        // 3: On met à jour allMessages
        setAllMessages([...allMessages, newGroup]);
        setMustSaveData(true);
      
        // 4: On met à jour l'id avec le nouvel id de groupe
        setId(newId);
      
        // Réinitialisation des paramètres
        setSearchText("");
        setFiltreEpoque([]);
      
        // 5: On remet à zéro l'état de sélection et on quitte le mode groupe
        setIdPhilosophes([]);
        setAddGroupMode(false);
        setSidebarVisible(false);
      };
      

    return (
        <>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div>
                {!addGroupMode ?
                <>
                    <div className="titreClassSearch largeShow">Discussions</div>
                    <div className="titreClassSearch mobileShow">PhiloGPT</div>
                    </>
                    :
                    <div className="titreClassSearch" style={{    display: 'flex',flexDirection: 'row'}}>
                        <div className="flecheRetour" onClick={() => setAddGroupMode(false)}>
                        <span data-icon="back" className="iconeSVG" style={{cursor:'pointer'}}><svg viewBox="0 0 24 24" height="30" width="30" preserveAspectRatio="xMidYMid meet" className="" version="1.1" x="0px" y="0px"><title>back</title><path fill="currentColor" d="M12,4l1.4,1.4L7.8,11H20v2H7.8l5.6,5.6L12,20l-8-8L12,4z"></path></svg></span>

                        </div>
                        <div>
          <div className="nouveauGroupe">Nouveau groupe</div>
          <div className="nbPhilosophes">{idPhilosophes.length} philosophe{idPhilosophes.length > 1 ? "s" : ""}</div>
                    </div>
                    </div>
                }
                </div>
                <div className="allIcones">
                    {!addGroupMode &&
                    <div className="iconeMenu" onClick={() => setAddGroupMode(true)}>
                        <span data-icon="new-chat-outline">
                            <svg viewBox="0 0 24 24" height="24" width="24" preserveAspectRatio="xMidYMid meet" style={{paddingTop: '4px'}} fill="none"><title>Nouveau groupe</title><path d="M9.53277 12.9911H11.5086V14.9671C11.5086 15.3999 11.7634 15.8175 12.1762 15.9488C12.8608 16.1661 13.4909 15.6613 13.4909 15.009V12.9911H15.4672C15.9005 12.9911 16.3181 12.7358 16.449 12.3226C16.6659 11.6381 16.1606 11.0089 15.5086 11.0089H13.4909V9.03332C13.4909 8.60007 13.2361 8.18252 12.8233 8.05119C12.1391 7.83391 11.5086 8.33872 11.5086 8.991V11.0089H9.49088C8.83941 11.0089 8.33411 11.6381 8.55097 12.3226C8.68144 12.7358 9.09947 12.9911 9.53277 12.9911Z" fill="currentColor"></path><path fillRule="evenodd" clipRule="evenodd" d="M0.944298 5.52617L2.99998 8.84848V17.3333C2.99998 18.8061 4.19389 20 5.66665 20H19.3333C20.8061 20 22 18.8061 22 17.3333V6.66667C22 5.19391 20.8061 4 19.3333 4H1.79468C1.01126 4 0.532088 4.85997 0.944298 5.52617ZM4.99998 8.27977V17.3333C4.99998 17.7015 5.29845 18 5.66665 18H19.3333C19.7015 18 20 17.7015 20 17.3333V6.66667C20 6.29848 19.7015 6 19.3333 6H3.58937L4.99998 8.27977Z" fill="currentColor"></path></svg>
                        </span>
                    </div>
}
                </div>
            </div>
            {addGroupMode &&
                <CreateGroup
                    chatPrompts={chatPrompts}
                    idPhilosophes={idPhilosophes}
                    setIdPhilosophes={setIdPhilosophes}
                    smallImages={smallImages}
                />
            }
            <div className="chatSearch">
                <div className="searchZone">
                    <div className="inSearchZone">
                        <div className="iconesSearch">
                            {searchText === "" ? (
                                <span data-icon="search" className="iconeSVG iSearch">
                                    <svg
                                        viewBox="0 0 24 24"
                                        height="24"
                                        width="24"
                                        preserveAspectRatio="xMidYMid meet"
                                        className=""
                                        version="1.1"
                                        x="0px"
                                        y="0px"
                                    >
                                        <title>search</title>
                                        <path
                                            fill="currentColor"
                                            d="M15.009,13.805h-0.636l-0.22-0.219c0.781-0.911,1.256-2.092,1.256-3.386 c0-2.876-2.332-5.207-5.207-5.207c-2.876,0-5.208,2.331-5.208,5.207s2.331,5.208,5.208,5.208c1.293,0,2.474-0.474,3.385-1.255 l0.221,0.22v0.635l4.004,3.999l1.194-1.195L15.009,13.805z M10.201,13.805c-1.991,0-3.605-1.614-3.605-3.605 s1.614-3.605,3.605-3.605s3.605,1.614,3.605,3.605S12.192,13.805,10.201,13.805z"
                                        ></path>
                                    </svg>
                                </span>
                            ) : (
                                <span data-icon="back" className="iconeSVG iFleche" onClick={() => setSearchText("")}>
                                    <svg
                                        viewBox="0 0 24 24"
                                        height="24"
                                        width="24"
                                        preserveAspectRatio="xMidYMid meet"
                                        className=""
                                        version="1.1"
                                        x="0px"
                                        y="0px"
                                    >
                                        <title>back</title>
                                        <path
                                            fill="currentColor"
                                            d="M12,4l1.4,1.4L7.8,11H20v2H7.8l5.6,5.6L12,20l-8-8L12,4z"
                                        ></path>
                                    </svg>
                                </span>
                            )}
                        </div>
                        
                        <input
                            type="text"
                            value={searchText}
                            onChange={(e) => setSearchText(e.target.value)}
                            className="inputSearch"
                            placeholder="Rechercher"
                        />
                    </div>
                </div>

                <DropdownFilter setFiltreEpoque={setFiltreEpoque} addGroupMode={addGroupMode}/>
                <Sidebar
                    prompt={prompt}
                    id={id}
                    setId={setId}
                    chatPrompts={chatPrompts}
                    allMessages={allMessages}
                    searchText={searchText}
                    filtreEpoque={filtreEpoque}
                    idPhilosophes={idPhilosophes}
                    onSelectPhilosophe={handleSelectPhilosophe}
                    addGroupMode={addGroupMode}
                    setLatestId={setLatestId}
                    smallImages={smallImages}
                />

            </div>

                    <div className={`create-group-arrow ${idPhilosophes.length > 1 ? "OKactif" : "OKinactif"}`} onClick={handleCreateGroup}>
                        <span data-icon="arrow-forward" style={{marginTop: '3px',marginLeft: '2px'}}><svg viewBox="0 0 30 30" height="30" width="30" preserveAspectRatio="xMidYMid meet" className="" version="1.1" x="0px" y="0px" enableBackground="new 0 0 30 30"><title>arrow-forward</title><path fill="currentColor" d="M15,7l-1.4,1.4l5.6,5.6H7v2h12.2l-5.6,5.6L15,23l8-8L15,7z"></path></svg></span>
                    </div>
        </>
    );
};

export default ChatSearch;
