import React from 'react';
import ReactDOM from 'react-dom';
import './ConfirmationModal.css';

const ConfirmationModal = ({ titre, texteBouton, onConfirm, onCancel }) => {
    const handleClickOutside = (event) => {
        event.stopPropagation();
      if (event.target.className === 'modal-overlay') {
        onCancel();
      }
    };
  
    return ReactDOM.createPortal(
      <div className="modal-overlay" onClick={handleClickOutside}>
        <div className="modal-content" onClick={(e) => e.stopPropagation()}>
          <div className="titreModal">{titre}</div>
          <div className="modal-buttons">
            <div className="cancel-button" onClick={onCancel}>Annuler</div>
            <div className="confirm-button" onClick={onConfirm}>{texteBouton}</div>
          </div>
        </div>
      </div>,
      document.body
    );
  };
  
  export default ConfirmationModal;