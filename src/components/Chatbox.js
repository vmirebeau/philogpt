import React, { useState, useEffect, useRef, useCallback, useMemo } from 'react';
import './ChatBox.css';
import { debounce } from 'lodash';
import Header from './Header';
import MessageInput from './MessageInput';
import MessageList from './ChatComposants/MessageList';
import Suggestions from './Suggestions';
import { useChatHandlers } from './ChatComposants/UseChatHandlers';
import getLongueurComplexitePrompt from './getLongueurComplexitePrompt';

function Chatbox({
  inputUser,
  setInputUser,
  prompt,
  philosopher,
  messages = [],
  setMessages = () => { },
  setShowInfos,
  setSidebarVisible,
  chatPrompts,
  setIdShowInfosWhenGroup,
  stopGen,
  setPhilosopher,
  latestId,
  setId,
  allMessages,
  setAllMessages,
  smallImages,
  allMessagesPhilosopher,
  setAllMessagesPhilosopher,
  setMustSaveData,
  setShowSources,
  setCurrentSources
}) {
  const [thinking, setThinking] = useState(false);
  const [generating, setGenerating] = useState(false);
  const [stopAll, setStopAll] = useState(null);
  const [showChevron, setShowChevron] = useState(false);
  const cadreMessagesRef = useRef(null);  // Référence pour cadreMessages
  const cadreHeightRef = useRef(null);  // Stocker la hauteur

  const [userInfo, setUserInfo] = useState(() => {
    const storedUserInfo = localStorage.getItem('userInfo');
    if (storedUserInfo) {
      try {
        return JSON.parse(storedUserInfo);
      } catch (error) {
        console.error("Erreur lors du parsing de userInfo depuis localStorage", error);
      }
    }
    // Valeur par défaut si rien n'est trouvé dans le localStorage
    return { niveau_reponses: 2, longueur_reponses: 2 };
  });
/*
  useEffect(() => {
    const storedUserInfo = localStorage.getItem('userInfo');
    if (storedUserInfo) {
      try {
        setUserInfo(JSON.parse(storedUserInfo));
      } catch (error) {
        console.error('Erreur lors du parsing de userInfo depuis localStorage', error);
      }
    }
  }, []);*/

  useEffect(() => {
    localStorage.setItem('userInfo', JSON.stringify(userInfo));
  }, [userInfo]);

  const messagesRef = useRef(null);
  const isNearBottomRef = useRef(true);
  const [reponsesEnCours, setReponsesEnCours] = useState(false);
  const prevScrollTopRef = useRef(0);

  const scrollDebut = useRef(0);

  const [stopScroll, setStopScroll] = useState(false);
  const [firstAssistantTop, setFirstAssistantTop] = useState(null); // État pour stocker la valeur top

  const [promptLongueur, setPromptLongueur] = useState("");

  useEffect(() => {
    setPromptLongueur(getLongueurComplexitePrompt(userInfo));
  }, [userInfo])
  // Utiliser useChatHandlers pour gérer l'envoi de messages

  const { handleSendMessage } = useChatHandlers({
    messages,
    philosopher,
    setMessages,
    stopAll,
    setStopAll,
    stopGen,
    setThinking,
    setGenerating,
    chatPrompts,
    setReponsesEnCours,
    setMustSaveData,
    promptLongueur
  });


  const updateCadreHeight = useCallback(() => {
    if (cadreMessagesRef.current) {
      cadreHeightRef.current = cadreMessagesRef.current.clientHeight;  // Met à jour la hauteur
    }
  }, []);

  useEffect(() => {
    const messagesDiv = messagesRef.current;
    if (messagesDiv) {
    const isUserNearBottom = // on en a besoin pour le chevron
    messagesDiv.scrollHeight - messagesDiv.scrollTop <= messagesDiv.clientHeight + 50;
  isNearBottomRef.current = isUserNearBottom;
  setShowChevron(!isUserNearBottom);
    }
  }, [messages]);

  useEffect(() => {
    // Récupérer la hauteur initiale de cadreMessages
    updateCadreHeight();

    // Ajoute un listener pour détecter le redimensionnement de la fenêtre
    window.addEventListener('resize', updateCadreHeight);

    // Nettoyage du listener lors du démontage du composant
    return () => {
      window.removeEventListener('resize', updateCadreHeight);
    };
  }, [updateCadreHeight]);  // Le useEffect dépend de la fonction updateCadreHeight

  useEffect(() => {
  // Trouver l'élément correspondant dans allMessages
  const correspondingMessage = allMessages.find((msg) => msg.id === philosopher?.id);

  // Si l'élément est trouvé et que unread est vrai, mettre à jour philosopher et allMessages
  if (correspondingMessage && correspondingMessage.unread) {
      setAllMessages((prevAllMessages) =>
        prevAllMessages.map((p) =>
          p.id === philosopher.id ? { ...p, unread: false } : p
        )
      );
    }
  }, [philosopher, setPhilosopher, setAllMessages]);

  useEffect(() => { //quand on commence une génération, on n'a pas de raison d'arrêter le scrolling
    if (reponsesEnCours) setStopScroll(false);
  }, [reponsesEnCours])

  const scrollToBottom = useCallback(() => {
    if (messagesRef.current) {
      messagesRef.current.scrollTop = messagesRef.current.scrollHeight;
    }
  }, []);


  const hardScrollToBottom = useCallback(() => {
    if (messagesRef.current) {
      //console.log("hardScrollToBottom");
      const targetPosition = messagesRef.current.scrollHeight;
      messagesRef.current.scrollTo({
        top: targetPosition,
        behavior: 'instant'
      });
    }
  }, []);

  const handleScroll = useCallback(
    debounce(() => {
      const messagesDiv = messagesRef.current;
  
      if (messagesDiv) {
        const currentScrollTop = messagesDiv.scrollTop;
  
        // Si l'utilisateur a fait remonter le scroll (position actuelle plus haute que la précédente)
        if (currentScrollTop < prevScrollTopRef.current) {
          setStopScroll(true);
        }
  
        // Met à jour la position précédente
        prevScrollTopRef.current = currentScrollTop;
  
        const isUserNearBottom =
          messagesDiv.scrollHeight - messagesDiv.scrollTop <= messagesDiv.clientHeight + 50;
        isNearBottomRef.current = isUserNearBottom;
        setShowChevron(!isUserNearBottom);
      }
    }, 200),
    [messagesRef, prevScrollTopRef, isNearBottomRef, setShowChevron]
  );

  const handlePhilosopherClick = useCallback(
    (name) => {
      const philosopherData = chatPrompts.find((p) => p.nom === name);
      if (philosopherData) {
        //console.log(philosopherData.id)
        setIdShowInfosWhenGroup(philosopherData.id);
      }
      setShowInfos(true);
    },
    [chatPrompts, setIdShowInfosWhenGroup, setShowInfos]
  );

useEffect(() => {
  const messagesDiv = messagesRef.current;
  if (messagesDiv) {
    // Si une réponse est en cours, forcer l'autoscroll
    if (reponsesEnCours) {
      hardScrollToBottom();
    } else if (!stopScroll) {
      const currentScrollTop = messagesDiv.scrollTop;
      if (currentScrollTop + 20 >= firstAssistantTop) {
        setStopScroll(true);
        messagesDiv.scrollTop = firstAssistantTop;
      } else {
        hardScrollToBottom();
      }
    }
  }
}, [messages, hardScrollToBottom, firstAssistantTop, stopScroll, reponsesEnCours]);

  useEffect(() => {
    hardScrollToBottom();
  }, [philosopher?.id, hardScrollToBottom])

  useEffect(() => {
    // On scrolle en bas dès qu'on commence la génération
    if (reponsesEnCours) {
      //console.log("on scrolle en bas");
      if (messagesRef.current) scrollDebut.current = messagesRef.current.scrollTop;
      hardScrollToBottom();

    }
  }, [reponsesEnCours, hardScrollToBottom]);

  // Mémorisation de la liste des messages pour éviter les re-rendus inutiles
  const renderedMessages = useMemo(() => (
    <MessageList
      messages={messages}
      philosopher={philosopher}
      smallImages={smallImages}
      handlePhilosopherClick={handlePhilosopherClick}
      thinking={thinking}
      setFirstAssistantTop={setFirstAssistantTop}
      reponsesEnCours={reponsesEnCours}
      setShowSources={setShowSources}
      setCurrentSources={setCurrentSources}
    />
  ), [messages, philosopher, smallImages, handlePhilosopherClick, thinking, reponsesEnCours]);

  
  return (
    <>
      <Header
        philosopher={philosopher}
        setShowInfos={setShowInfos}
        setSidebarVisible={setSidebarVisible}
        chatPrompts={chatPrompts}
        setIdShowInfosWhenGroup={setIdShowInfosWhenGroup}
        messages={messages}
        setMessages={setMessages}
        latestId={latestId}
        setId={setId}
        allMessages={allMessages}
        setAllMessages={setAllMessages}
        smallImages={smallImages}
        setMustSaveData={setMustSaveData}
      />
      <div className="cadreMessages" ref={cadreMessagesRef}  style={{ position: 'relative', overflowY: 'hidden' }}>
        <div className="messages" ref={messagesRef} onScroll={handleScroll} style={{ overflowY: 'auto', maxHeight: '100%' }}>
          {renderedMessages}
          <div style={{ height: '20px', flexShrink: 0 }} />
        </div>
        <div className={`chevronScroll ${showChevron ? 'chevronVisible' : 'chevronHidden'}`} onClick={scrollToBottom}>
          <svg viewBox="0 0 30 30" height="30" width="30" preserveAspectRatio="xMidYMid meet" style={{ transform: 'rotate(90deg)' }} x="0px" y="0px">
            <title>chevron</title>
            <path fill="currentColor" d="M11,21.212L17.35,15L11,8.65l1.932-1.932L21.215,15l-8.282,8.282L11,21.212z"></path>
          </svg>
        </div>
      </div>
      <div className="message-input-container">
      <Suggestions prompt={prompt} philosopher={philosopher} chatPrompts={chatPrompts} />
        <MessageInput
          onSendMessage={handleSendMessage}
          thinking={thinking}
          generating={generating}
          stopAll={stopAll}
          inputUser={inputUser}
          setInputUser={setInputUser}
          philosopher={philosopher}
          userInfo={userInfo}
          setUserInfo={setUserInfo}
        />
      </div>
    </>
  );
}

export default Chatbox;


