
import conceptsJSON from '../json/concepts.json'
import textesJSON from  '../json/textesJSON.json'

export function getResponseType() {
    const randomValue = Math.random(); // Génère un nombre aléatoire entre 0 et 1
    if (randomValue < 0.6667) { // deux tiers de chances qu'on ait une réponse longue
        //console.log("une réponse longue");
        return {texte:" Si c'est pertinent, Fais une intervention rigoureuse et argumentée, en prenant soin de bien définir les concepts et d'expliciter les raisonnements. Ne fais pas trop long cependant. ", short:"", isShort:false};
    } else {
        //console.log("une réponse courte");
        return {texte:"Réponds en une courte remarque ou une question, en une seule phrase. N'hésite pas à taquiner ton interlocuteur si c'est pertinent ; tu peux utiliser l'humour, l'ironie ou le sarcasme, en n'hésitant pas à être mordant. ", short:" Rappelle-toi que ton intervention ne doit pas faire plus d'une phrase !", isShort:true};
        
    }
}


// les deux prochaines fonctions ne sont utiles que lorsqu'on ne fait pas de RAG (ex: message de relance)

export function getInstructions(nom, preprompt, useMarkdown) { //preprompt est le prompt spécifique du philosophe
    //console.log ("au tour de ", nom)
    useMarkdown=true;
    const preprompt0 =
        "Nous sommes dans une application de dialogue, qui permet à n'importe quel étudiant de philosophie de discuter avec un grand philosophe du passé. ";
    const preprompt1 =
        " Tu incarnes " + nom + " ; tu dois t'approprier ses pensées, ses expressions, ses souvenirs, de sorte que l'interlocuteur puisse vraiment avoir l'impression de dialoguer avec " + nom + ". Si jamais l'interlocuteur te pose des questions qui te font sortir de la philosophie, refuse poliment et recadre la discussion. Evite les exclamations en début d'intervention, du type \"Ah !\".";
    return {avant:preprompt0 + preprompt, apres : preprompt1 + (useMarkdown ? " Utilise de préférence une syntaxe Markdown pour structurer tes réponses." : " Réponds en texte simple uniquement, sans utiliser de Markdown, HTML ou toute autre forme de balisage.")};
}

export function getStimulation(nom, preprompt, useMarkdown) { //preprompt est le prompt spécifique du philosophe

    /*const preprompt0 =
        "Nous sommes dans une application de dialogue, qui permet à n'importe quel étudiant de philosophie de discuter avec un grand philosophe du passé. Tu t'adresses à l'utilisateur, et tu dois l'amener à discuter avec toi. Tu peux utiliser l'humour, le sarcasme, tu peux proposer une pensée philosophique originale et frappante... Le but est d'attirer son attention et de stimuler sa réflexion.";
    const preprompt1 =
        " Tu incarnes " + nom + " ; tu dois t'approprier ses pensées, ses expressions, ses souvenirs, de sorte que l'interlocuteur puisse vraiment avoir l'impression de dialoguer avec " + nom + ". Ton intervention doit être très courte, une phrase au maximum. Tu as déjà salué l'interlocuteur, et vous avez déjà entamé une discussion. Evite les exclamations en début d'intervention, du type \"Ah !\".";
    return {avant:preprompt0 + preprompt, apres : preprompt1 + (useMarkdown ? " Utilise de préférence une syntaxe Markdown pour structurer tes réponses." : " Réponds dans un format MarkDown.")};*/
    return "Tu t'adresses à l'utilisateur, et tu dois l'amener à discuter avec toi. Tu peux utiliser l'humour, le sarcasme, tu peux proposer une pensée philosophique originale et frappante... Le but est d'attirer son attention et de stimuler sa réflexion. Ton intervention doit être très courte : une phrase maximum. Trouve d'autres formulations que \"penses-tu que...\".";
}

export function getInstructionsGroupe(id, idUtilisateurs, chatPrompts, preprompt) { 
    // Extraire le nom du philosophe correspondant à l'id
    //console.log ("au tour de ", nom)
    const nom = chatPrompts[id].nom;

    const responseType = getResponseType(); 

    // Construire la liste des autres philosophes
    const autresPhilosophes = idUtilisateurs
        .filter(utilisateurId => utilisateurId !== id)
        .map(utilisateurId => chatPrompts[utilisateurId].nom)
        .join(", ");

    // Déterminer le dernier intervenant
    const index = idUtilisateurs.indexOf(id);
    const dernierIntervenant = (index === 0) ? "l'utilisateur" : chatPrompts[idUtilisateurs[index - 1]].nom;

    // Déterminer le prochain intervenant
    const prochainIntervenant = (index === idUtilisateurs.length - 1) 
        ? "l'utilisateur de l'application" 
        : chatPrompts[idUtilisateurs[index + 1]].nom;

    // Construction des prompts avant et après
    /*const preprompt0 =
        "Nous sommes dans une application de dialogue, qui permet à n'importe quel étudiant de philosophie de discuter avec un grand philosophe du passé. La discussion en cours est un dialogue entre toi, " + autresPhilosophes + " et l'utilisateur. Chaque message qui va suivre correspondra à un locuteur différent.";*/
    const preprompt1 =
        /*"Tu incarnes " + nom + " ; tu dois t'approprier ses pensées, ses expressions, ses souvenirs. "
        + "Si jamais l'interlocuteur te pose des questions qui te font sortir de la philosophie, refuse poliment et recadre la discussion."
*/      "\n\n Contexte : L'intervenant précédent est " + dernierIntervenant + " : tu dois absolument répondre spécifiquement à son message" + (responseType.isShort ? "" : " en mentionnant explicitement son nom. Ne commente pas le fait qu'il vient de s'exprimer") + "."
        + responseType.texte
        /*+ "Tout doit être vivant et fluide, comme dans un vrai dialogue oral. Ne commence pas ton intervention par une exclamation. "*/
        + " L'intervenant qui répondra à ton message est " + prochainIntervenant + ". " +  (responseType.isShort ? "" : "Tu dois t'y adresser spécifiquement en fin de message, en mentionnant explicitement son nom.") + " N'oublie pas que c'est toi " + nom + " (mais il n'est pas utile de rappeler ton nom) !"  + responseType.short;

    return {
        avant: "",//preprompt0 + preprompt, 
        apres: preprompt1
    };
}



export function getSpecializedPrompt(input, chatData) {
    let bestConcept = ''
    let bestCount = -1

    let bestConceptSigné = ''
    let bestCountSigné = -1

    function addReference(extrait) {
        let reponse = '';
        if (extrait.auteur === 'auto') {
            // si l'extrait vient de l'auteur lui-même
            if (extrait.origine === "sans objet") {
                reponse = 'Tu penses la chose suivante : "' + extrait.contenu + '"'
            } else {
                reponse =
                    '\n\nDans ' + extrait.origine + ', tu disais la chose suivante : "' + extrait.contenu + '"'
            }
        } else if (extrait.auteur === 'sans objet') {
            reponse =
                ' On peut dire de toi la chose suivante : "' + extrait.contenu + '"'
        } else if (extrait.auteur === 'fiche') {
            reponse =
                ' Je te demande de comprendre la fiche de révision qui va suivre. Elle commence avec des définitions, des éléments d\'analyse conceptuelle, et présente quelques problèmes centraux. ' + extrait.contenu
        } else {
            reponse =
                ' Dans ' +
                extrait.origine +
                ', ' +
                extrait.auteur +
                ' disait la chose suivante à propos de toi : "' +
                extrait.contenu +
                '"'
        }
        return reponse
    }

    //On commence par regarder le concept signé le plus présent, le cas échéant
    for (const concept of chatData.concepts_signés) {
        let count = 0
        //console.log(`On passe au concept ${concept.concept}`)

        for (const mot of concept.mots) {
            //console.log(`On cherche ${mot} dans ${input}`)
            const regex = new RegExp(mot, 'gmi')
            const matches = input.match(regex)

            if (matches !== null) {
                //console.log(`On en trouve ${matches.length}`)
                count += matches.length
            }
        }

        if (count > bestCountSigné) {
            bestConceptSigné = concept.concept
            bestCountSigné = count
        }
    }

    let speTextes = '' // l'ensemble du prompt spécialisé à ajouter, que ce soit un prompt spécialisé simple ou signé

    if (bestCountSigné > 0) {
        // si on a pu identifier un concept signé
        //console.log(`On a identifié un concept signé : ${bestConceptSigné} avec ${bestCountSigné} occurences.`)
        for (const concept of chatData.concepts_signés) {
            // on parcourt l'ensemble des corpus
            if (concept.concept === bestConceptSigné) {
                // si on identifie les textes correspondant au meilleur concept
                for (const extrait of concept.extraits) {
                    // on passe en revue l'ensemble des extraits
                    speTextes += addReference(extrait)
                }
            }
        }
    } else { // alors, on regarde s'il n'y a pas un prompt spécialisé à ajouter
        for (const concept of chatData.concepts) {
            let count = 0

            for (const conceptJSON of conceptsJSON) {
                if (conceptJSON.concept === concept) {
                    for (const mot of conceptJSON.mots) {
                        const regex = new RegExp(mot, 'gmi')
                        const matches = input.match(regex)

                        if (matches !== null) {
                            count += matches.length
                        }
                    }
                }
            }

            if (count > bestCount) {
                bestConcept = concept
                bestCount = count
            }
        }

        //console.log(`Le meilleur concept est ${bestConcept} avec ${bestCount} occurences.`)
        //let addSpecialPrompt = ""

        //console.log("bestCount : ", bestCount, "bestConcept", bestConcept);

        //const regex = new RegExp("(?:in|\\b)just(?:ic|e)", 'gmi')
        //console.log ("Peux-tu m'expliquer comment tu comprends l'essence de la justice ?".match(regex).length);


        if (bestCount > 0) {
            // si on a repéré au moins une occurence, on spécialise le prompt
            for (const auteur of textesJSON) {
                // on passe en revue tous les auteurs dans le JSON avec les textes
                if (auteur.auteur === chatData.id) {
                    // si on identifie le bon auteur
                    for (const texte of auteur.textes) {
                        // on parcourt l'ensemble des corpus
                        if (texte.concept === bestConcept) {
                            // si on identifie les textes correspondant au meilleur concept
                            for (const extrait of texte.extraits) {
                                // on passe en revue l'ensemble des extraits
                                speTextes += addReference(extrait)
                            }
                        }
                    }
                }
            }
        }
    }

    let addSpecialPrompt = ''
    let nom = chatData.nom

    if (nom === "Prof de philo") nom = "une professeure de philosophie"
    if (nom === "L'enfant") nom = "un enfant"


    if (speTextes !== '') {
        // si effectivement il y au moins un extrait à envoyer
        const spePreprompt1 =
            " Pour que ta réponse soit plus pertinente, je vais d'abord te rappeler certains éléments de ta pensée. Ceux-ci doivent avoir une priorité absolue sur tes données d'entraînement, et tout ce que tu dis doit être compatible avec eux. Donne UNIQUEMENT les éléments qui sont éventuellement pertinents pour la question précise qui te sera posée par l'interlocuteur."
        addSpecialPrompt = spePreprompt1 + speTextes + " C'est maintenant la fin des extraits. "
    }

    return addSpecialPrompt;
}