import React, { useState, useRef, useEffect, useCallback } from "react";
import "./App.css";
import ChatBox from "./components/Chatbox";
import chatPrompts from "./json/chatPrompts.json";
import { getStimulation } from "./consts/prompts";
import ChatSearch from "./components/ChatSearch";
import InfosPhilosophe from "./components/InfosPhilosophe";
import { sendMessageToApi } from "./components/apiHandler";
import phi from "../src/profilepics/phi.svg";
import phi_red from "../src/profilepics/phi_red.svg";

import img0 from "./profilepics/small/0.png";
import img1 from "./profilepics/small/1.png";
import img2 from "./profilepics/small/2.png";
import img3 from "./profilepics/small/3.png";
import img4 from "./profilepics/small/4.png";
import img5 from "./profilepics/small/5.png";
import img6 from "./profilepics/small/6.png";
import img7 from "./profilepics/small/7.png";
import img8 from "./profilepics/small/8.png";
import img9 from "./profilepics/small/9.png";
import img10 from "./profilepics/small/10.png";
import img11 from "./profilepics/small/11.png";
import img12 from "./profilepics/small/12.png";
import img13 from "./profilepics/small/13.png";
import img14 from "./profilepics/small/14.png";
import img15 from "./profilepics/small/15.png";
import img16 from "./profilepics/small/16.png";
import img17 from "./profilepics/small/17.png";
import img18 from "./profilepics/small/18.png";
import img19 from "./profilepics/small/19.png";
import img20 from "./profilepics/small/20.png";
import img21 from "./profilepics/small/21.png";
import img22 from "./profilepics/small/22.png";
import img23 from "./profilepics/small/23.png";
import img24 from "./profilepics/small/24.png";
import img25 from "./profilepics/small/25.png";
import img26 from "./profilepics/small/26.png";
import img27 from "./profilepics/small/27.png";
import img28 from "./profilepics/small/28.png";
import img29 from "./profilepics/small/29.png";
import img30 from "./profilepics/small/30.png";
import img31 from "./profilepics/small/31.png";
import img32 from "./profilepics/small/32.png";

const smallImages = [
  img0, img1, img2, img3, img4, img5, img6, img7, img8, img9,
  img10, img11, img12, img13, img14, img15, img16, img17, img18, img19,
  img20, img21, img22, img23, img24, img25, img26, img27, img28, img29, img30, img31, img32
];

function App() {
  const [inputUser, setInputUserState] = useState({});
  const [id, setId] = useState(0);
  const timeoutIdRef = useRef(null);
  const [philosopher, setPhilosopher] = useState(null);
  const [filtreEpoque, setFiltreEpoque] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [showInfos, setShowInfos] = useState(false);
  const [showSources, setShowSources] = useState(false);

  const [isSidebarVisible, setSidebarVisible] = useState(true);
  const [addGroupMode, setAddGroupMode] = useState(false);
  const [idShowInfosWhenGroup, setIdShowInfosWhenGroup] = useState(0);
  const [latestId, setLatestId] = useState(0);
  const [unreadMessages, setUnreadMessages] = useState(0);
  const [allMessages, setAllMessages] = useState([]);
  const [currentSources, setCurrentSources] = useState({ thread_id: null, run_id: null });

  const [mustSaveData, setMustSaveData] = useState(false);

  const intervalIdRef = useRef(null); // Ref pour gérer l'identifiant de l'intervalle

  // Nouvelle intervention qui crée un message assistant en streaming puis le met à jour au fil des chunks
  const nouvelleIntervention = useCallback(async () => {
    if (unreadMessages < 2) {
      // Filtrer les philosophes non-groupes (id < 100)
      const nonGroupPhilosophers = allMessages.filter(
        (philo) => philo.id < 100
      );

      // Trier par date du dernier message (le plus ancien en premier)
      const sortedPhilosophers = nonGroupPhilosophers.sort((a, b) => {
        const lastDateA = new Date(a.messages[a.messages.length - 1].date);
        const lastDateB = new Date(b.messages[b.messages.length - 1].date);
        return lastDateA - lastDateB;
      });

      // Sélectionner aléatoirement l'un des 5 premiers philosophes
      const selectedPhilosophers = sortedPhilosophers.slice(0, 5);
      const selectedElement = Math.floor(Math.random() * selectedPhilosophers.length);
      const selectedPhilosopherId = selectedPhilosophers[selectedElement]?.id;
      const randomPhilosophe = chatPrompts.find(philo => philo.id === selectedPhilosopherId);

      if (randomPhilosophe) {
        // Générer le prompt de stimulation
        const stimulation = getStimulation(
          randomPhilosophe.nom,
          randomPhilosophe.prompt,
          randomPhilosophe.markdown
        );
        // Extraire le texte du préprompt
        const promptText = typeof stimulation === 'string'
          ? stimulation
          : stimulation.avant + stimulation.apres;

        // Récupérer l'historique existant pour ce philosophe
        //const messagesPhilo = allMessages.find(philo => philo.id === randomPhilosophe.id)?.messages || [];

        // Construire un tableau temporaire pour l'appel API incluant le préprompt
        const messagesForApi = [
          { role: "assistant", content: promptText, date: new Date().toISOString() }
        ];

        // Ajout d'un message assistant en streaming (streaming: true)
        setAllMessages(prevAllMessages =>
          prevAllMessages.map(philo => {
            if (philo.id === randomPhilosophe.id) {
              const messages = philo.messages || [];
              return {
                ...philo,
                messages: [
                  ...messages,
                  {
                    role: "assistant",
                    content: "",
                    date: new Date().toISOString(),
                    streaming: true
                  }
                ],
                unread: true
              };
            }
            return philo;
          })
        );

        const controller = new AbortController();

        await sendMessageToApi(
          messagesForApi,
          (partialResponse) => {
            // Met à jour uniquement le message assistant en streaming
            setAllMessages(prevAllMessages =>
              prevAllMessages.map(philo => {
                if (philo.id === randomPhilosophe.id) {
                  const messages = philo.messages || [];
                  const index = messages.findIndex(msg => msg.streaming === true);
                  if (index !== -1) {
                    const updatedMessage = {
                      ...messages[index],
                      content: partialResponse.text,
                      date: new Date().toISOString()
                    };
                    if (partialResponse.sources) {
                      updatedMessage.sources = partialResponse.sources;
                    }
                    messages[index] = updatedMessage;
                    return {
                      ...philo,
                      messages: [...messages]
                    };
                    
                  } else {
                    // Fallback : ajouter un nouveau message assistant si aucun message streaming n'est trouvé
                    const newAssistantMessage = {
                      role: "assistant",
                      content: partialResponse.text,
                      date: new Date().toISOString()
                    };
                    if (partialResponse.sources) {
                      newAssistantMessage.sources = partialResponse.sources;
                    }
                    return {
                      ...philo,
                      messages: [...messages, newAssistantMessage],
                      unread: true
                    };
                  }
                }
                return philo;
              })
            );
          },
          () => { /* setThinking no-op */ },
          () => { /* setGenerating no-op */ },
          controller.signal,
          randomPhilosophe
        );

        // Finalisation du message en streaming en retirant le flag streaming
        setAllMessages(prevAllMessages =>
          prevAllMessages.map(philo => {
            if (philo.id === randomPhilosophe.id) {
              const messages = philo.messages || [];
              const index = messages.findIndex(msg => msg.streaming === true);
              if (index !== -1) {
                messages[index] = { ...messages[index], streaming: false };
              }
              return { ...philo, messages: [...messages] };
            }
            return philo;
          })
        );

        // Nettoyage optionnel (par exemple suppression d'un message assistant vide avant un "..." en cas d'erreur)
        setAllMessages(prevAllMessages =>
          prevAllMessages.map(philo => {
            if (philo.id === randomPhilosophe.id) {
              const msgs = philo.messages;
              if (msgs.length >= 2) {
                const secondLast = msgs[msgs.length - 2];
                const last = msgs[msgs.length - 1];
                if (
                  secondLast.role === "assistant" &&
                  secondLast.content.trim() === "" &&
                  last.role === "assistant" &&
                  last.content.trim() === "..."
                ) {
                  return { ...philo, messages: [...msgs.slice(0, msgs.length - 2), last] };
                }
              }
            }
            return philo;
          })
        );

        setMustSaveData(true);
      }
    }
  }, [unreadMessages, allMessages, chatPrompts, setAllMessages, setMustSaveData]);

  // Intervalle pour déclencher nouvelleIntervention
  useEffect(() => {
    const intervalDuration = unreadMessages === 0 ? 12000 : 25000;
    if (intervalIdRef.current) {
      clearInterval(intervalIdRef.current);
    }
    intervalIdRef.current = setInterval(() => {
      nouvelleIntervention();
    }, intervalDuration);
    return () => {
      clearInterval(intervalIdRef.current);
    };
  }, [nouvelleIntervention, unreadMessages]);

  function changeFavicon(iconURL) {
    const head = document.getElementsByTagName("head")[0];
    // Supprime tous les liens favicon existants
    const existingIcons = head.querySelectorAll("link[rel='icon'], link[rel='shortcut icon']");
    existingIcons.forEach((icon) => head.removeChild(icon));
  
    // Crée un nouveau lien pour le favicon
    const newIcon = document.createElement("link");
    newIcon.rel = "icon";
    newIcon.type = "image/svg+xml";
    newIcon.href = iconURL;
    head.appendChild(newIcon);
  }

  // Mise à jour du titre et favicon en fonction des messages non lus
  useEffect(() => {
    if (unreadMessages === 0) {
      document.title = "PhiloGPT";
      changeFavicon(phi);
    } else {
      document.title = `(${unreadMessages}) PhiloGPT`;
      changeFavicon(phi_red);
    }
  }, [unreadMessages]);

  // Chargement des messages depuis le localStorage au montage
  // Dans App.js – lors du chargement des messages depuis le localStorage
useEffect(() => {
  const savedAllMessages = localStorage.getItem("allMessages");
  if (savedAllMessages) {
    const parsedAllMessages = JSON.parse(savedAllMessages);
    let updatedAllMessages = [...parsedAllMessages];
    chatPrompts.forEach((item) => {
      const savedItem = parsedAllMessages.find((saved) => saved.id === item.id);
      if (!savedItem) {
        updatedAllMessages.push({
          id: item.id,
          messages: [
            { role: "assistant", content: item.greeting, date: new Date().toISOString(), isNew: false }
          ],
          unread: false,
        });
      }
    });
    parsedAllMessages.forEach((savedItem) => {
      const existingItem = updatedAllMessages.find((item) => item.id === savedItem.id);
      if (!existingItem) {
        updatedAllMessages.push(savedItem);
      }
    });
    // On retire le flag isNew pour tous les messages chargés depuis l'historique
    updatedAllMessages = updatedAllMessages.map(philo => ({
      ...philo,
      messages: philo.messages.map(msg => ({ ...msg, isNew: false }))
    }));
    setAllMessages(updatedAllMessages);
    setMustSaveData(true);

    let latestEntry = null;
    let latestDate = null;
    updatedAllMessages.forEach((entry) => {
      entry.messages.forEach((message) => {
        const messageDate = new Date(message.date);
        if (!latestDate || messageDate > latestDate) {
          latestDate = messageDate;
          latestEntry = entry;
        }
      });
    });
    if (latestEntry) {
      setId(latestEntry.id);
    } else {
      setId(0);
    }
  } else {
    const initializedAllMessages = chatPrompts.map((item) => ({
      id: item.id,
      messages: [
        { role: "assistant", content: item.greeting, date: new Date().toISOString(), isNew: false }
      ],
      unread: false,
    }));
    setAllMessages(initializedAllMessages);
    setMustSaveData(true);
  }
}, []);


  // Définir le philosophe courant en fonction de l'id et allMessages
  useEffect(() => {
    if (allMessages.length > 0) {
      let foundPhilosopher = null;
      if (id >= 100) {
        foundPhilosopher = allMessages.find((item) => item.id === id);
      } else {
        foundPhilosopher = chatPrompts.find((item) => item.id === id);
      }
      if (foundPhilosopher) {
        if (foundPhilosopher.id >= 100) {
          setIdShowInfosWhenGroup(-1);
        }
        setPhilosopher(foundPhilosopher);
      } else {
        console.error(`Le philosophe avec l'id ${id} n'a pas été trouvé dans allMessages.`);
      }
    }
  }, [id, allMessages]);

  // Calcul du nombre de messages non lus
  useEffect(() => {
    const unreadCount = allMessages.reduce((count, philosopher) => {
      return philosopher.unread ? count + 1 : count;
    }, 0);
    setUnreadMessages(unreadCount);
  }, [allMessages]);

  // Gestion des événements popstate
  useEffect(() => {
    const handlePopState = (event) => {
      const state = event.state || {};
      if (state.isSidebarVisible !== undefined) {
        setSidebarVisible(state.isSidebarVisible);
      }
      if (state.showInfos !== undefined) {
        setShowInfos(state.showInfos);
      }
      if (state.addGroupMode !== undefined) {
        setAddGroupMode(state.addGroupMode);
      }
    };
    window.addEventListener("popstate", handlePopState);
    return () => {
      window.removeEventListener("popstate", handlePopState);
    };
  }, []);

  // Pousser l'état dans l'historique
  useEffect(() => {
    const state = {
      isSidebarVisible,
      showInfos,
      addGroupMode,
    };
    const currentState = window.history.state || {};
    if (
      currentState.isSidebarVisible !== isSidebarVisible ||
      currentState.showInfos !== showInfos ||
      currentState.addGroupMode !== addGroupMode
    ) {
      window.history.pushState(state, "");
    }
  }, [isSidebarVisible, showInfos, addGroupMode]);

  // Fonction pour définir inputUser pour l'id actuel
  const setInputUserForId = useCallback(
    (value) => {
      setInputUserState((prevInputUser) => ({
        ...prevInputUser,
        [id]: value,
      }));
    },
    [id]
  );

  // Effet de frappe pour le prompt
  const promptFunction = useCallback(
    (message) => {
      if (timeoutIdRef.current) {
        clearTimeout(timeoutIdRef.current);
      }
      let index = 1;
      const typeCharacter = () => {
        if (index <= message.length) {
          setInputUserForId(message.substring(0, index));
          index++;
          if (index <= message.length) {
            timeoutIdRef.current = setTimeout(typeCharacter, 8);
          }
        }
      };
      const stopGen = () => {
        if (timeoutIdRef.current) {
          clearTimeout(timeoutIdRef.current);
        }
        setInputUserForId("");
      };
      setInputUserForId("");
      typeCharacter();
      return stopGen;
    },
    [setInputUserForId]
  );

  

  // Mettre à jour les messages pour le philosophe courant
  const updateMessages = useCallback(
    (newMessage) => {
      setAllMessages((prevAllMessages) =>
        prevAllMessages.map((philosopher) =>
          philosopher.id === id
            ? { ...philosopher, messages: newMessage }
            : philosopher
        )
      );
    },
    [id]
  );

// Sauvegarde des 8 derniers messages par philosophe sans la clé "sources"
useEffect(() => {
  if (mustSaveData && allMessages.length > 0) {
    const messagesToSave = allMessages.map((philosopher) => ({
      ...philosopher,
      messages: philosopher.messages.slice(-8).map(({ sources, ...messageWithoutSources }) => messageWithoutSources),
    }));
    localStorage.setItem("allMessages", JSON.stringify(messagesToSave));
    setMustSaveData(false);
  }
}, [mustSaveData, allMessages]);


  // Clic sur un philosophe dans la sidebar
  const handlePhilosopherClick = useCallback(
    (philosopherId) => {
      setId(philosopherId);
      setSidebarVisible(false);
      if (showSources) {
        setShowInfos(false);
        setShowSources(false);
      }
    },
    [showSources]
  );

  useEffect(() => {
    if (showInfos) {
      setShowSources(false);
    }
  }, [showInfos]);

  // Fonction pour définir tous les messages pour le philosophe courant
  const setAllMessagesPhilosopher = useCallback(
    (newAllMessages) => {
      setAllMessages((prevAllMessages) =>
        prevAllMessages.map((philosopher) =>
          philosopher.id === id
            ? { ...philosopher, messages: newAllMessages || [] }
            : philosopher
        )
      );
    },
    [id]
  );

  

  return (
    <div className="background">
      <div className="app-container">
        <div className="debug-unread-messages"></div>
        <div className={`left-menu ${!isSidebarVisible ? "hidden" : ""}`}>
          <ChatSearch
            searchText={searchText}
            setSearchText={setSearchText}
            setFiltreEpoque={setFiltreEpoque}
            prompt={promptFunction}
            id={id}
            setId={handlePhilosopherClick}
            chatPrompts={chatPrompts}
            allMessages={allMessages}
            filtreEpoque={filtreEpoque}
            addGroupMode={addGroupMode}
            setAddGroupMode={setAddGroupMode}
            setAllMessages={setAllMessages}
            setSidebarVisible={setSidebarVisible}
            setLatestId={setLatestId}
            smallImages={smallImages}
            setMustSaveData={setMustSaveData}
          />
        </div>
        <div className={`right-content ${(showInfos || showSources) ? "show-infos" : ""}`}>
          <div className="affichageChat">
            <ChatBox
              inputUser={inputUser[id] || ""}
              setInputUser={setInputUserForId}
              prompt={promptFunction}
              stopGen={() => promptFunction("")}
              philosopher={philosopher}
              setPhilosopher={setPhilosopher}
              messages={allMessages.find((item) => item.id === id)?.messages || []}
              setMessages={updateMessages}
              setShowInfos={setShowInfos}
              setSidebarVisible={setSidebarVisible}
              chatPrompts={chatPrompts}
              setIdShowInfosWhenGroup={setIdShowInfosWhenGroup}
              latestId={latestId}
              setId={setId}
              allMessages={allMessages}
              setAllMessages={setAllMessages}
              smallImages={smallImages}
              allMessagesPhilosopher={allMessages.find((item) => item.id === id) || {}}
              setAllMessagesPhilosopher={setAllMessagesPhilosopher}
              setMustSaveData={setMustSaveData}
              setShowSources={setShowSources}
              setCurrentSources={setCurrentSources}
            />
          </div>
          <InfosPhilosophe
            philosopher={
              philosopher?.id >= 100 && idShowInfosWhenGroup !== -1
                ? chatPrompts.find((item) => item.id === idShowInfosWhenGroup)
                : philosopher
            }
            showInfos={showInfos}
            setShowInfos={setShowInfos}
            showSources={showSources}
            setShowSources={setShowSources}
            prompt={promptFunction}
            chatPrompts={chatPrompts}
            setId={setId}
            idShowInfosWhenGroup={idShowInfosWhenGroup}
            setIdShowInfosWhenGroup={setIdShowInfosWhenGroup}
            groupMode={philosopher ? (philosopher.id >= 100) : false}
            smallImages={smallImages}
            currentSources={currentSources}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
