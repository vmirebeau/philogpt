import React, { useEffect, useState, useRef } from 'react';
import './Sources.css';
import { createPortal } from "react-dom";
import copy_img from "./profilepics/copy.svg";
import { saveAs } from 'file-saver';
import { Document, Packer, Paragraph, TextRun, AlignmentType } from 'docx';

const Sources = ({ source, setShowSources, setShowInfos }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [loading, setLoading] = useState(true);
  const [openMenu, setOpenMenu] = useState(false);
  const [menuStyle, setMenuStyle] = useState({});
  const menuRef = useRef(null);
  const buttonRef = useRef(null);

  // Calculer la position du menu
  const handleClickOutside = (event) => {
    if (
      menuRef.current &&
      !menuRef.current.contains(event.target) &&
      buttonRef.current &&
      !buttonRef.current.contains(event.target)
    ) {
      setOpenMenu(false);
    }
  };

  useEffect(() => {
    if (buttonRef.current) {
      const rect = buttonRef.current.getBoundingClientRect();
      setMenuStyle({
        top: rect.bottom + window.scrollY,
        left: rect.left + window.scrollX - 200,
        width: rect.width + 200,
      });
    }
  }, []);

  useEffect(() => {
    const handleResize = () => {
      setOpenMenu(false);
      if (buttonRef.current) {
        const rect = buttonRef.current.getBoundingClientRect();
        setMenuStyle({
          top: rect.bottom + window.scrollY,
          left: rect.left + window.scrollX - 200,
          width: rect.width + 200,
        });
      }
    };

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  useEffect(() => {
    const updateMenuPosition = () => {
      if (buttonRef.current) {
        const rect = buttonRef.current.getBoundingClientRect();
        setMenuStyle({
          top: rect.bottom + window.scrollY,
          left: rect.left + window.scrollX - 200,
          width: rect.width + 200,
        });
      }
    };

    updateMenuPosition();
    window.addEventListener('resize', updateMenuPosition);
    return () => window.removeEventListener('resize', updateMenuPosition);
  }, [openMenu]);

  useEffect(() => {
    if (openMenu) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, [openMenu]);

  // Ici, on ne fait plus d'appel réseau puisque la prop "source" contient directement le tableau des sources.
  useEffect(() => {
    setLoading(false);
  }, [source]);

  // Si la prop "source" est déjà un tableau, on l'utilise directement.
  // Sinon, on tente de lire source.sources.
  const results = Array.isArray(source) ? source : (source?.sources || []);
  // Limiter l'affichage à 5 sources maximum
  const limitedResults = results.slice(0, 5);

  const formatText = (text) => {
    return text
      .replace(/(\r\n|\n){2,}/g, '<br /><br />')
      .replace(/(\r\n|\n)/g, '<br />');
  };

  const createParagraphsFromText = (text) => {
    const lines = text.split(/(\r\n|\n)/);
    return lines
      .filter(line => line.trim() !== '')
      .map((line, idx) => new Paragraph({
        children: [new TextRun({ text: line })],
        alignment: AlignmentType.LEFT,
        spacing: { after: 200 },
      }));
  };

  const exportSourcesToDocx = (sources, filename) => {
    const doc = new Document({
      sections: [
        {
          children: sources.flatMap((result, index) => [
            new Paragraph({
              children: [
                new TextRun({
                  text: 'Source ' + (index + 1),
                  bold: true,
                  size: 28,
                }),
              ],
              alignment: AlignmentType.CENTER,
              spacing: { after: 400 },
            }),
            ...createParagraphsFromText(result.page_content),
            result.metadata && result.metadata.filename ? new Paragraph({
              children: [
                new TextRun({
                  text: result.metadata.filename.slice(0, result.metadata.filename.lastIndexOf('.')).replace(/_/g, ' '),
                  italics: true,
                })
              ],
              alignment: AlignmentType.RIGHT,
              spacing: { after: 200 },
            }) : null,
          ]).filter(p => p !== null),
        },
      ],
    });

    Packer.toBlob(doc).then((blob) => {
      saveAs(blob, `${filename}.docx`);
    });
  };

  const exportSource = () => {
    if (!limitedResults[activeIndex]) return;
    exportSourcesToDocx([limitedResults[activeIndex]], `source ${activeIndex + 1}`);
  };

  const exportAllSources = () => {
    exportSourcesToDocx(limitedResults, 'toutes les sources');
  };


  const menuPortalElement = document.getElementById('menu-portal') || document.createElement('div');
  if (!menuPortalElement.id) {
    menuPortalElement.id = 'menu-portal';
    document.body.appendChild(menuPortalElement);
  }

  return (
    <>
      <div className="titreEtCroix" style={{ backgroundColor: 'white' }}>
        <div style={{ display: 'flex', flex: 1, justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <span
              data-icon="x"
              className="fermerCroix"
              onClick={() => { setShowSources(false); setShowInfos(false); }}
              style={{ display: 'flex' }}
            >
              <svg viewBox="0 0 24 24" height="24" width="24" preserveAspectRatio="xMidYMid meet" fill="currentColor">
                <title>x</title>
                <path d="M19.6004 17.2L14.3004 11.9L19.6004 6.60005L17.8004 4.80005L12.5004 10.2L7.20039 4.90005L5.40039 6.60005L10.7004 11.9L5.40039 17.2L7.20039 19L12.5004 13.7L17.8004 19L19.6004 17.2Z"></path>
              </svg>
            </span>
            <div className="titre">Sources de la réponse</div>
          </div>
          <div>
            {limitedResults.length > 0 && (
              <img
                src={copy_img}
                style={{ height: '24px', width: '24px', cursor: 'pointer' }}
                onClick={(e) => { e.stopPropagation(); setOpenMenu(!openMenu); }}
                ref={buttonRef}
              />
            )}
          </div>
        </div>
      </div>
      <div className="allInfos boxInfos" style={{ alignItems: 'flex-start', backgroundColor: '#f4f4f4', boxShadow: 'none' }}></div>
      <div>
        {limitedResults.length === 0 ? (
          <div className="no-sources-message">
            <div style={{ color: 'black' }}>Aucune source disponible pour cette réponse.</div>
          </div>
        ) : (
          <>
            <div className="buttons-container">
              {limitedResults.map((_, index) => (
                <button
                  key={index}
                  className={`source-button ${activeIndex === index ? 'active' : ''}`}
                  onClick={() => setActiveIndex(index)}
                >
                  {index + 1}
                </button>
              ))}
            </div>
            {limitedResults[activeIndex] && (
              <div className="source-content">
                <div className="source">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: formatText(limitedResults[activeIndex].page_content),
                    }}
                  />
                  {limitedResults[activeIndex].metadata && limitedResults[activeIndex].metadata.filename && (
                    <div style={{ textAlign: 'right', fontStyle: 'italic', userSelect: 'text' }}>
                      {limitedResults[activeIndex].metadata.filename
                        .slice(0, limitedResults[activeIndex].metadata.filename.lastIndexOf('.'))
                        .replace(/_/g, ' ')}
                    </div>
                  )}
                </div>
              </div>
            )}
          </>
        )}
      </div>
      {createPortal(
        <div
          className={`menu ${openMenu ? "displayMenu" : "hideMenu"}`}
          style={menuStyle}
          ref={menuRef}
        >
          <div className="menu-item" onClick={(e) => { e.stopPropagation(); setOpenMenu(false); exportSource(); }}>
            Exporter la source
          </div>
          <div className="menu-item" onClick={(e) => { e.stopPropagation(); setOpenMenu(false); exportAllSources(); }}>
            Exporter toutes les sources
          </div>
        </div>,
        menuPortalElement
      )}
    </>
  );
};

export default Sources;
