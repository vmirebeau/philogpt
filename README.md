## Infos générales

PhiloGPT est une application web en React destinée à simuler une discussion avec les grands auteur.ices de la philosophies, via des chatbots appuyés sur le modèle Llama-3.3-70B-Instruct-Turbo (via Deep Infra). Cette application intègre un système de RAG, grâce aux API Cohere et Pinecone.

Une version déployée peut être trouvée ici : [http://philogpt.vmirebeau.fr](http://philogpt.vmirebeau.fr)

N'hésitez pas à me contacter à vmirebeau.dev@gmail.com

## Pour faire tourner ce programme

Le dossier /serveur/ ne participera bien sûr pas au build React. Il faudra le copier tel quel à la racine de votre serveur. Un exemple de .htaccess est à trouver dans ce dossier, il faut essayer de jouer avec les paramètres pour permettre le streaming des réponses.

Vous aurez besoin d'une clé OpenAI. Créez ensuite un fichier /serveur/.env , contenant :
OPENAI_API_KEY=[votre clé API]

Il faut ajouter un fichier /src/components/encoder/encodeJS.js et un fichier /serveur/encoder/encodePHP.php, qui doivent chacun définir une fonction similaire encode(x), x étant une chaîne. Il faut simplement que les deux fonctions encode renvoient la même réponse. Dans l'idéal, encode doit prendre en paramètre l'input, pour créer une clé de contrôle. Si vous ne voulez pas sécuriser votre fichier proxy.php, vous pouvez faire retourner une constante, mais ça permettra à n'importe qui de faire usage de votre API via proxy.php.

Le serveur avec lequel le proxy communique appelle les API de Cohere, Pinecone et Deep Infra, le tout à travers Langchain. Un autre repo sur mon compte de la Forge présente ce type de code.


## Infos React : scripts disponibles

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).